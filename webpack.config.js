const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = {
  entry: './src/index.js',
  // mode: 'production', // ModySwitch
  mode: 'development',
  output: {
    filename: '../wordpress/wp-content/themes/99oz/js/bundle.js',
    // filename: 'bundle.js',
    path: path.resolve(__dirname, './public/'),
    publicPath: './', // ModySwitch
    // publicPath: '/',
  },
  watch: true,
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/env'],
            plugins: [
              '@babel/plugin-proposal-class-properties',
              '@babel/plugin-proposal-object-rest-spread',
              '@babel/plugin-proposal-export-default-from',
              '@babel/plugin-proposal-export-namespace-from',
            ],
          },
        },
      }, {
        test: /\.s?css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: process.env.WEBPACK_MODE === 'development',
            }
          },
          {loader: 'css-loader', options: {importLoaders: 1, sourceMap: true}},
          {loader: 'postcss-loader', options: {}},
          {loader: 'sass-loader', options: {sourceMap: true}},
        ],
      }, {
        test: /\.(woff(2)?|ttf|eot|otf|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [{
          loader: 'file-loader',
          options: {
            publicPath: function (url) {
              return './' + url;
            },
            name: 'fonts/[folder]/[name].[ext]',
          },
        }],
      }, {
        test: /node_module.*\.(png|svg|jpg|gif)$/,
        use: [{
          loader: 'file-loader',
          options: {
            publicPath: function (url) {
              return './' + url;
            },
            name: 'assets/[folder]/[name].[ext]',
          },
        }],
      }, {
        test: /\.(png|svg|jpg|gif)$/,
        exclude: /node_modules/,
        use: [{
          loader: 'file-loader',
          options: {
            publicPath: function (url) {
              return './' + url;
            },
            name: '[path][name].[ext]',
            context: path.resolve(__dirname, './src/html'),
          },
        }],
      },
    ],
  },
  optimization: {
    sideEffects: false,
    minimizer: [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})],
  },
  plugins: [
    new webpack.SourceMapDevToolPlugin({
      filename: '[file].map',
      exclude: ['/vendor/']
    }),
    new MiniCssExtractPlugin({
      filename: "../wordpress/wp-content/themes/99oz/style.css",
    }),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: path.resolve(__dirname, './src/html/home.html'),
    }),
    new HtmlWebpackPlugin({
      filename: 'checkout.html',
      template: path.resolve(__dirname, './src/html/checkout.html'),
    }),
    new HtmlWebpackPlugin({
      filename: 'single-product.html',
      template: path.resolve(__dirname, './src/html/single-product.html'),
    }),
    new HtmlWebpackPlugin({
      filename: 'shop.html',
      template: path.resolve(__dirname, './src/html/shop.html'),
    }),
    new HtmlWebpackPlugin({
      filename: 'faqs.html',
      template: path.resolve(__dirname, './src/html/faqs.html'),
    }),
    new HtmlWebpackPlugin({
      filename: 'cart.html',
      template: path.resolve(__dirname, './src/html/cart.html'),
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
      slick: 'slick-carousel',
      masonry: 'masonry-layout',
      imagesloaded: 'imagesloaded',
      jqueryBridge: 'jquery-bridget',
    }),
  ],
  // devtool: 'source-map', // ModySwitch
  devtool: 'cheap-module-eval-source-map',
  devServer: {
    contentBase: path.resolve(__dirname, './public'),
    historyApiFallback: true,
  },
};