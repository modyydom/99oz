=== WooCommerce variations Select2 ===
Contributors: laugei
Tags: woocommerce, variations, select2
Requires at least: 4.6
Tested up to: 4.9.6
Stable tag: 1.1
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

A simple plugin that enables Select2 for WooCommerce variations select boxes

== Description ==
This plugin allows WooCommerce shop owners to change variations drop-down list native design to custom, which looks the same on all browsers and platforms.

The plugin uses Select2 javascript library which is included with WooCommerce.

== Installation ==
Upload this plugin to your website and activate it.

You\'re done!

== Screenshots ==
1. Before and after.

== Changelog ==
### 1.1

* Added support for multisite.

### 1.0.2

* Changed selector for better compatibility with other plugins.

### 1.0.1

* Fixed dropdown position issue.

### 1.0

* Initial version.
