jQuery(document).ready(function(e) {
    e.each(e(".single-product select[data-attribute_name]"), function(t, n) {
        e(n).select2({
            minimumResultsForSearch: -1,
            dropdownParent: e(n).closest(".variations")
        })
    })
});