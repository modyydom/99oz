<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Okanagan
 */

get_header();
if (have_posts()) {
	the_post();
}
$categories = get_the_category(get_the_ID());
?>
	
	<div class="blog-entry-page-wrapper">
		<div class="page-header">
			<div class="row">
				<div class="col-md-6 col-sm-12">
					<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="iv-wp-from-left no-zoom">
				</div>
				<div class="col-md-6 col-sm-12">
					<div class="our-commitment">
						<img class="opacity-background" src="./assets/images/blog-entry/background-with-opacity.png" alt="">
						<h2 class="our-commitment-h2 iv-wp"><?php the_title(); ?></h2>
						<p class="data-and-time iv-wp-from-right"><span class="apr"><?php echo get_the_date('M j Y'); ?></span> <span>///</span><?php echo $categories[0]->name; ?></p>
						<div class="share">
							<div class="links iv-wp-from-right">
								<span class="spans">share</span>
								<span class="this spans">this</span>
								<span class="article spans">article</span>
							</div>
							<div class="icons iv-wp-from-right">
								<a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=570َ');return false;" target="_new" href="https://www.facebook.com/sharer/sharer.php?kid_directed_site=0&amp;sdk=joey&amp;u=<?php the_permalink() ?>&amp;display=popup&amp;ref=plugin&amp;src=share_button" title="Share on Facebook.">
									<i class="fab fa-facebook-f"></i>
								</a>
								<a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" target="_new" href="http://twitter.com/home/?status=<?php the_title(); ?> - <?php the_permalink(); ?>" title="Tweet this!">
									<i class="fab fa-twitter"></i>
								</a>
								<a onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" target="_new" href="http://www.linkedin.com/shareArticle?mini=true&amp;title=<?php the_title(); ?>&amp;url=<?php the_permalink(); ?>" title="Share on LinkedIn">
									<i class="fab fa-linkedin-in"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="our-commitment-to-quality">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-md-7 col-12">
						<div class="description iv-wp-from-bottom">
							<?php the_content(); ?>
						</div>
						<div class="more-topics">
							<h5 class="iv-wp-from-left">Tags</h5>
							<div class="buttons-flex">
								<?php $all_tags = wp_get_post_tags(get_the_ID());
								foreach ($all_tags as $tag) { ?>
									<a href="<?php $tag->link; ?>" class="iv-wp-from-left same-style"><?php echo $tag->name; ?></a>
								<?php }
								?>
							</div>
						</div>
						<div class="comments iv-wp-from-bottom">
							<h2 class="comments-h2 iv-wp-from-left">Comments</h2>
							<?php echo do_shortcode('[gs-fb-comments]'); ?>
							<!--							<h5 class="comments-number iv-wp-from-left">2 Comments</h5>-->
							<!--							<div class="commenter-element">-->
							<!--								<div class="comment-writer">-->
							<!--									<h2 class="main-h2 iv-wp-from-bottom">thchelsey</h2>-->
							<!--									<p class="data-and-timeivb-->
							<!--"><span class="apr">Mar 24 2019</span> <span>///</span> 3:20 AM-->
							<!--									</p>-->
							<!--								</div>-->
							<!--								<div class="comment">-->
							<!--									<h4 class="comment-title iv-wp-from-bottom">Thanks for the article! </h4>-->
							<!--									<p class="comment-body iv-wp">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do-->
							<!--										eiusmod tempor-->
							<!--										incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.-->
							<!--									</p>-->
							<!--									<div class="replay hover-arrow iv-wp-from-bottom"><span>replay</span>-->
							<!--										<i class="fal fa-long-arrow-right"></i>-->
							<!--									</div>-->
							<!--								</div>-->
							<!--							</div>-->
							<!--							<div class="commenter-element">-->
							<!--								<div class="comment-writer">-->
							<!--									<h2 class="main-h2 iv-wp-from-bottom">james_22</h2>-->
							<!--									<p class="data-and-timeivb-->
							<!--"><span class="apr">Mar 24 2019</span> <span>///</span> 3:20 AM-->
							<!--									</p>-->
							<!--								</div>-->
							<!--								<div class="comment">-->
							<!--									<h4 class="comment-title iv-wp-from-bottom">Good one</h4>-->
							<!--									<p class="comment-body iv-wp">Consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>-->
							<!--									<div class="replay hover-arrow iv-wp-from-bottom"><span>replay</span>-->
							<!--										<i class="fal fa-long-arrow-right"></i>-->
							<!--									</div>-->
							<!--								</div>-->
							<!--							</div>-->
							<!--							<div class="leave">-->
							<!--								<h5 class="leave-a-replay iv-wp-from-top">leave a replay:</h5>-->
							<!--								<p class="your-email iv-wp-from-top">Your email will not be published.</p>-->
							<!--							</div>-->
							<!--							<div class="contact-form">-->
							<!--								-->
							<!--								<div class="contact-me">-->
							<!--									<div class="form-contact">-->
							<!--										<from class="contact-form">-->
							<!--											<div class="row">-->
							<!--												<div class="col-md-6">-->
							<!--													<label for="first-name" class="iv-wp-from-left input-label">first name</label>-->
							<!--													<input type="text" class="iv-wp-from-left first-name" id="first-name">-->
							<!--												</div>-->
							<!--												<div class="col-md-6">-->
							<!--													<label for="last-name" class="iv-wp-from-left input-label">last name</label>-->
							<!--													<input type="text" class="iv-wp-from-left last-name" id="last-name">-->
							<!--												</div>-->
							<!--											</div>-->
							<!--											-->
							<!--											<label for="subject" class="iv-wp-from-left input-label">subject</label>-->
							<!--											<input type="text" name="subject" class="iv-wp-from-left subject" id="subject">-->
							<!--											-->
							<!--											<label for="your-message" class="iv-wp-from-left input-label">your message</label>-->
							<!--											<textarea name="your message" class="iv-wp-from-left your-message" id="your-message"></textarea>-->
							<!--											<div class="submit-button hover-arrow">-->
							<!--												<input type="submit" class="submit">-->
							<!--												<i class="fal fa-long-arrow-right"></i>-->
							<!--											</div>-->
							<!--										</from>-->
							<!--									</div>-->
							<!--								</div>-->
							<!--							</div>-->
						</div>
					</div>
					<div class="col-lg-4 col-md-5 col-12">
						<form action="" class="news-letter hover-arrow iv-wp-from-right">
							<h3 class="title-with-line">
								<span class="text">JOIN OUR NEWSLETTER</span>
								<span class="line"></span>
							</h3>
							<input type="email" placeholder="email address">
							<button>
								SIGN UP
								<i class="fal fa-long-arrow-right"></i>
							</button>
						</form>
						<h3 class="title-with-line iv-wp-from-right">
							<span class="text">JOIN OUR NEWSLETTER</span>
							<span class="line"></span>
						</h3>
						<?php
						//for use in the loop, list 5 post titles related to first tag on current post
						$related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 2, 'post__not_in' => array($post->ID) ) );
						if( $related ) {
							foreach( $related as $post ) {
						setup_postdata($post);
									$categories = get_the_category(get_the_ID());
									?>
									<div class="hover-arrow blog-feed iv-wp-from-right free-height cbd-card colored-top-border brown1">
										<div class="img-container">
											<img src="assets/images/blog/blog-img-2.png" alt="">
										</div>
										<div class="card-bottom">
											<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
											<h3><?php the_title(); ?></h3>
											</a>
											<div class="date-reading">
												<span class="date"><?php echo get_the_date( 'M j Y' ); ?></span>
												&nbsp///&nbsp
												<span class="reading"><?php echo $categories[0]->name; ?></span>
											</div>
											<p><?php echo wp_trim_words(get_the_content(),20) ?></p>
											<i class="fal fa-long-arrow-right"></i>
										</div>
									</div>
								<?php }
							wp_reset_query();
						}
						?>
					</div>
					<div class="keep-explore">
						<div class="container">
							<div class="separator thick iv-wp-from-top">
								<div class="text"><?php echo the_field('signle_page_bottom_section_title', 'option'); ?></div>
							</div>
							
							<div class="products">
								<div class="row justify-content-around align-items-center">
									<?php
									if (have_rows('single_page_bottom_section', 'option')):
										while (have_rows('single_page_bottom_section', 'option')) : the_row();
											$item_title = get_sub_field('item_title');
											$item_image = get_sub_field('item_image');
											$item_link = get_sub_field('item_link');
											?>
											
											<div class="col-12 col-md-6 col-lg-4">
												<div class="overflow-hidden iv-wp-from-right hover-arrow product colored-top-border brown1">
													<img src="<?php echo $item_image['url']; ?>" alt="<?php echo $item_image['alt']; ?>">
													<a href="<?php echo $item_link; ?>">
														<h2><?php echo $item_title; ?>
														</h2>
														<i class="fal fa-long-arrow-right"></i>
													</a>
												</div>
											</div>
										
										<?php endwhile;
									endif;
									?>
								
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	
	</div>
	<div class="container separator end-of-page thick"></div>

<?php
get_sidebar();
get_footer();
