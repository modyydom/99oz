<?php
/*
 * template name: shop page
 */
get_header(); ?>
  <div class="shop-page-wrapper">
    <div class="shop-hero" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/shop/shop-header-bg.png')">
      <div class="container">
        <h4 class="iv-wp">EXPERIENCE THE DIFFERENCE</h4>
        <h2 class="iv-wp">OUR PRODUCTS</h2>
        <p class="iv-wp">This is where a statement, introducing the products will be displayed. Lorem ipsum dolor sit amet poenam consectitur</p>
      </div>
    </div>
    <div class="container">
      
      <section class="products-section">
        <div class="row justify-content-center justify-content-md-around">
          <?php
          $args = array('post_type'      => 'product',
                        'posts_per_page' => 3);
          $loop = new WP_Query($args);
          if ($loop->have_posts()) {
            while ($loop->have_posts()) : $loop->the_post(); ?>
              <div class="col-lg-4 col-md-5 col-sm-8 col-12 iv-wp-from-bottom">
                <?php
                wc_get_template_part('content', 'product');
                ?>
              </div>
            <?php
            endwhile;
          } else {
            echo __('No products found');
          }
          wp_reset_postdata();
          ?>
        </div>
      </section>
      <div class="separator iv-wp-from-top thick dark iv-wp-from-bottom"></div>
      
      <div class="more-options">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/shop/wholesales-icon.png" alt="" class=" iv-wp-from-bottom">
        <h3 class="iv-wp-from-right">CLICK HERE FOR WHOLESALE OPTIONS</h3>
        <i class="the-icon fal fa-long-arrow-right iv-wp-from-right"></i>
      </div>
      
      <section class="product-is-easy">
        <h3 class="section-title iv-wp-from-top">
          Easy to Use
        </h3>
        <div class="separator iv-wp-from-top"></div>
        <div class="row">
          <div class="col-lg-6 col-sm-12">
            <div class="product-easy-big-wrapper">
              <div class="easy-wrapper spoon">
                <img src="<?php echo get_template_directory_uri(); ?>/wordpress/wp-content/themes/okanagan/assets/images/shop/h-spoon.png" alt="" class="iv-wp-from-top">
                <h4 class="iv-wp-from-top">STORAGE <span class="dot"></span></h4>
                <p class="details iv-wp">Keep at room temperature to preserve freshness.</p>
                <div class="more-details">
                  <h6 class="iv-wp-from-left">STORE AWAY FROM:</h6>
                  <p class="iv-wp-from-left"><span>///</span> humidity</p>
                  <p class="iv-wp-from-left"><span>///</span> heat</p>
                  <p class="iv-wp-from-left"><span>///</span> light</p>
                </div>
              </div>
              <div class="product-image iv-wp-from-bottom">
                <img src="<?php echo get_template_directory_uri(); ?>/wordpress/wp-content/themes/okanagan/assets/images/shop/v-spoon.png" alt="">
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-sm-12">
            <div class="product-easy-big-wrapper dropper">
              <div class="product-image dropper iv-wp-from-bottom">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/shop/shop-easy-to-use-dropper.png" alt="">
              </div>
              <div class="easy-wrapper dropper">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/shop/easy-to-use_2.png" alt="" class="iv-wp-from-top">
                <h4 class="iv-wp-from-top">DROPPER</h4>
                <p class="details iv-wp">Our droppers are clearly marked to give you the correct serving size every time.</p>
                <div class="more-details">
                  <p class="iv-wp-from-right"><span>///</span> 1.00mL = 17mg</p>
                  <p class="iv-wp-from-right"><span>///</span> 0.75mL = 12.75mg</p>
                  <p class="iv-wp-from-right"><span>///</span> 0.50mL = 8.5mg</p>
                  <p class="iv-wp-from-right"><span>///</span> 0.25mL = 4.25mg</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
<?php get_footer(); ?>