<!-- Begin Sidebar -->
<form action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" id="filter">
    <div class="prices-filter-sidebar">
        <div class="sidebar-title">
            <span>PRICE</span>
        </div>
        <div class="prices-boxes">
            <?php getPricesAttributes(); ?>
        </div>

        <div class="divider"></div>
        <div class="section">
            <div class="sidebar-title">
                <span>TYPE</span>
            </div>
            <div class="row types-choises">
                <?php getTypesAttributes(); ?>
            </div>
        </div>
        <div class="divider"></div>
        <div class="section">
            <div class="sidebar-title">
                <span>EFFECTS</span>
            </div>
            <div class="checkboxes effects-filter-checkboxes">
                <!-- That's how we get Effects items for sidebar -->
                <?php getEffectsAttributes(); ?>
                <!-- End of text! -->
            </div>
        </div>
        <div class="divider"></div>
        <div class="section">
            <div class="sidebar-title">
                <span>USAGES</span>
            </div>
            <div class="checkboxes usages-filter-checkboxes">
                <!-- Guess what? We'll get usages items below -->
                <?php getUsagesAttributes(); ?>
                <!-- unfortunatly.. End of text! -->
            </div>
        </div>
        <div class="divider" style="margin-bottom:0px"></div>
        <button name="reset" class="reset-btn">Clear Filters</button>
        <div class="divider reset-btn-divider" style="margin-bottom:0px"></div>

        <!-- End of Sidebar -->
    </div>
    <button>Apply filter</button>
    <input type="hidden" name="action" value="myfilter">
</form>
<br><br><br>
<?php if (isset($_POST['submit'])) {

    $effects = $_POST['effects'];
    $usages = $_POST['usages'];
    $prices = $_POST['price_attributes'];
    $types = $_POST['types_attributes'];

}

