<?php
/**
 * Output a single payment method
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/payment-method.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<li class="iv-wp-from-bottom custom-checkbox_2 wc_payment_method payment_method_<?php echo esc_attr( $gateway->id ); ?>">
	
	<label class="checkbox-container toggleable contains-image toggleable-slide-up-header" for="payment_method_<?php echo esc_attr( $gateway->id ); ?>">
				<span class="label-text"><?php echo $gateway->get_title(); /* phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotEscaped */ ?></span>
		
<!--				<img alt="cards" class="cards-image" src="./assets/images/checkout/cards.png">-->
    
    <?php echo $gateway->get_icon(); /* phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotEscaped */ ?>
<!--		<h4 class="text-body">Safe money transfer using your bank account. Visa, Maestro, Discover, American Express.</h4>-->
		<input id="payment_method_<?php echo esc_attr( $gateway->id ); ?>" type="radio" name="payment_method" value="<?php echo esc_attr( $gateway->id ); ?>" <?php checked( $gateway->chosen, true ); ?> data-order_button_text="<?php echo esc_attr( $gateway->order_button_text ); ?>" >
		<span class="checkmark">
									<i class="after"></i>
								</span>
	</label>
	<div class="toggleable-slide-up-body">
		
    <?php if ( $gateway->has_fields() || $gateway->get_description() ) : ?>
			<div class="payment_box payment_method_<?php echo esc_attr( $gateway->id ); ?>">
        <?php $gateway->payment_fields(); ?>
			</div>
    <?php endif; ?>
	</div>
</li>



