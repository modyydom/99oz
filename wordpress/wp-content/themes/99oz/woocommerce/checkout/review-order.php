<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>



<div class="shop_table woocommerce-checkout-review-order-table">
<!--	<thead>-->
<!--		<tr>-->
<!--			<th class="product-name">--><?php //_e( 'Product', 'woocommerce' ); ?><!--</th>-->
<!--			<th class="product-total">--><?php //_e( 'Total', 'woocommerce' ); ?><!--</th>-->
<!--		</tr>-->
<!--	</thead>-->
  
  <?php
  do_action( 'woocommerce_review_order_before_cart_contents' );
  
  foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
    $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
    $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);
  
    if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key)) {
      $product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key);
      ?>
	
	    <div class="order-item iv-wp-from-right <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
		    <div class="product-img">
      
          <?php
          $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key);
      
          if (!$product_permalink) {
            echo $thumbnail; // PHPCS: XSS ok.
          } else {
            printf('<a href="%s">%s</a>', esc_url($product_permalink), $thumbnail); // PHPCS: XSS ok.
          }
          ?>
			    
			    
          <?php echo apply_filters( 'woocommerce_checkout_cart_item_quantity', ' <div class="quantity">' . sprintf( '%s', $cart_item['quantity'] ) . '</div>', $cart_item, $cart_item_key ); ?>
            <?php echo wc_get_formatted_cart_item_data( $cart_item ); ?>
		    </div>
		    <div class="order-info">
			    <h5 class="the-title"> <?php echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;'; ?></h5>
			    <h5 class="the-price"><?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); ?></h5>
		    </div>
	    </div>
      <?php
    }
  }
  
  do_action( 'woocommerce_review_order_after_cart_contents' );
  ?>
	
	<div class="subtotal-wrapper iv-wp-from-right">
		<div class="subtotal">
			<h4 class="the-text"><?php _e( 'Subtotal', 'woocommerce' ); ?>:</h4>
			<h4 class="the-price"><?php wc_cart_totals_subtotal_html(); ?></h4>
		</div>
    <?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
			<tr class="">
				<th></th>
				<td></td>
			</tr>
	    <div class="subtotal cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
		    <h4 class="the-text"><?php wc_cart_totals_coupon_label( $coupon ); ?>:</h4>
		    <h4 class="the-price"><?php wc_cart_totals_coupon_html( $coupon ); ?></h4>
	    </div>
    <?php endforeach; ?>
    
    <?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>
	    <table>
		    <tfoot>
      <?php do_action( 'woocommerce_review_order_before_shipping' ); ?>
      
      <?php wc_cart_totals_shipping_html(); ?>
      
      <?php do_action( 'woocommerce_review_order_after_shipping' ); ?>
		    </tfoot>
	    </table>
    <?php endif; ?>
    
    <?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
	    <div class="subtotal fee">
		    <h4 class="the-text"><?php echo esc_html( $fee->name ); ?>:</h4>
		    <h4 class="the-price"><?php wc_cart_totals_fee_html( $fee ); ?></h4>
	    </div>
    <?php endforeach; ?>
    
    <?php if ( wc_tax_enabled() && ! WC()->cart->display_prices_including_tax() ) : ?>
      <?php if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
        <?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : ?>
			    <div class="subtotal tax-rate tax-rate-<?php echo sanitize_title( $code ); ?>">
				    <h4 class="the-text"><?php echo esc_html( $tax->label ); ?>:</h4>
				    <h4 class="the-price"><?php echo wp_kses_post( $tax->formatted_amount ); ?></h4>
			    </div>
        <?php endforeach; ?>
      <?php else : ?>
		    <div class="subtotal tax-total">
			    <h4 class="the-text"><?php echo esc_html( WC()->countries->tax_or_vat() ); ?>:</h4>
			    <h4 class="the-price"><?php wc_cart_totals_taxes_total_html(); ?></h4>
		    </div>
      <?php endif; ?>
    <?php endif; ?>
	</div>
  
  
  <?php do_action( 'woocommerce_review_order_before_order_total' ); ?>
	
	<div class="total iv-wp-from-left">
		<h4 class="the-text"><?php _e( 'Total', 'woocommerce' ); ?></h4>
		<h4 class="the-price"><?php wc_cart_totals_order_total_html(); ?></h4>
	</div>
  
  <?php do_action( 'woocommerce_review_order_after_order_total' ); ?>
	
  
 
  
 
</div>
