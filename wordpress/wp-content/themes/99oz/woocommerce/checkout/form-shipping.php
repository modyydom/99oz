<?php
/**
 * Checkout shipping information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-shipping.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 * @global WC_Checkout $checkout
 */

defined('ABSPATH') || exit;
?>
<div class="woocommerce-shipping-fields">
  <?php if (true === WC()->cart->needs_shipping_address()) : ?>
		<div class="checkout-subtitle iv-wp-from-left shipping-subtitle"><?php esc_html_e('SHIPPING DETAILS', 'woocommerce'); ?></div>
		<div class="iv-wp-from-bottom custom-checkbox_2">
			<label class="checkbox-container toggleable">
				Same as shipping address
				<input checked type="radio" name="ship_to_different_address" value="0">
				<span class="checkmark">
									<i class="after"></i>
								</span>
			</label>
		</div>
		
		<div class="iv-wp-from-bottom custom-checkbox_2 toggled">
		<label class="checkbox-container toggleable toggleable-slide-up-header">
			Use different billing address
			<input id="ship-to-different-address-checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" <?php // checked( apply_filters( 'woocommerce_ship_to_different_address_checked', 'shipping' === get_option( 'woocommerce_ship_to_destination' ) ? 1 : 0 ), 1 ); ?> type="radio" name="ship_to_different_address" value="1" />
			<span class="checkmark">
									<i class="after"></i>
								</span>
		</label>
		<div class="toggleable-slide-up-body">
			
			<div class="shipping_address_mody">
        
        <?php do_action('woocommerce_before_checkout_shipping_form', $checkout); ?>
        
        
        <?php
        $fields = $checkout->get_checkout_fields('shipping');
        foreach ($fields as $key => $field) {
          
          switch ($key) {
            case 'shipping_first_name':
            case 'shipping_state':
              echo '<div class="form-row input-wrapper iv-wp-from-bottom  w-50-l">';
              woocommerce_form_field($key, $field, $checkout->get_value($key));
              
              break;
            case 'shipping_last_name':
            case 'shipping_postcode':
              echo '<div class="form-row input-wrapper iv-wp-from-bottom  w-50-r">';
              woocommerce_form_field($key, $field, $checkout->get_value($key));
              break;
            case 'shipping_country':
            case 'shipping_city':
            case 'shipping_address_1':
            case 'shipping_address_2':
              echo '<div class="form-row input-wrapper iv-wp-from-bottom ">';
              woocommerce_form_field($key, $field, $checkout->get_value($key));
              break;
            default:
              echo '<div class="d-none">';
              break;
          }
          echo '</div>';
        }
        ?>
        
        <?php do_action('woocommerce_after_checkout_shipping_form', $checkout); ?>
			
			</div>
		</div>
    <?php if (true === WC()->cart->needs_shipping_address()) : ?></div><?php endif; ?>
  <?php endif; ?>
</div>
<div class="woocommerce-additional-fields iv-wp-from-bottom">
  <?php do_action('woocommerce_before_order_notes', $checkout); ?>
  
  <?php if (apply_filters('woocommerce_enable_order_notes_field', 'yes' === get_option('woocommerce_enable_order_comments', 'yes'))) : ?>
    
    <?php if (!WC()->cart->needs_shipping() || wc_ship_to_billing_address_only()) : ?>
			<div class="toggleable-slide-up-header the-header">
				
				
				<h4 class="checkout-subtitle iv-wp-from-left contact-info-subtitle oz99-black-color" style="pointer-events: none"><?php esc_html_e('Additional information', 'woocommerce'); ?></h4>
			
			</div>
    <?php endif; ?>
		
		<div class="toggleable-slide-up-body the-body">
      <?php foreach ($checkout->get_checkout_fields('order') as $key => $field) : ?>
				<div class="input-wrapper iv-wp-from-bottom">
          <?php woocommerce_form_field($key, $field, $checkout->get_value($key)); ?>
				</div>
      <?php endforeach; ?>
		</div>
  
  <?php endif; ?>
  
  <?php do_action('woocommerce_after_order_notes', $checkout); ?>
</div>
