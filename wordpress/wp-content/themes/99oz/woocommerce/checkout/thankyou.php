<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     3.2.0
 */

if (!defined('ABSPATH')) {
  exit;
}
?>
<div class="thanks-popup-page-wrapper">
	<div class="container">
		<div class="woocommerce-order">
			<div class="thanks-popup-2 oz99-off-white-background-color">
				<div class="content-popup">
					<img class="iv-wp-from-top" src="<?php echo get_template_directory_uri() ?>/assets/images/thanks-pop/icon-thanks-pop.png" alt="">
          
          <?php if ($order) : ?>
            
            <?php if ($order->has_status('failed')) : ?>
							<h2 class="thanks-logo oz99-primary-color iv-wp-from-bottom">Opps!! an error occurred.</h2>
							
							<p
								class="iv-wp your-order oz99-black-color woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php _e('Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.',
                                                                                                                                 'woocommerce'); ?></p>
							
							<p class="iv-wp-from-bottom woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
								<a href="<?php echo esc_url($order->get_checkout_payment_url()); ?>" class="button pay">
									<button class="continue oz99-secondary-color oz99-off-white-background-color hover-arrow"><?php _e('Pay', 'woocommerce') ?>
										<i class="fal fa-long-arrow-right oz99-secondary-color"></i>
									</button>
								</a>
                <?php if (is_user_logged_in()) : ?>
									<a href="<?php echo esc_url(wc_get_page_permalink('myaccount')); ?>" class="button pay">
										<button class="iv-wp-from-bottom continue oz99-secondary-color oz99-off-white-background-color hover-arrow"><?php _e('My account', 'woocommerce'); ?>
											<i class="fal fa-long-arrow-right oz99-secondary-color"></i>
										</button>
									</a>
                <?php endif; ?>
							</p>
            
            <?php else : ?>
							<h2 class="thanks-logo oz99-primary-color iv-wp-from-top">THANK YOU FOR YOUR ORDER!</h2>
							
							<p class="iv-wp your-order oz99-black-color woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters('woocommerce_thankyou_order_received_text',
                                                                                                                                                      __('Your order has been received. Be sure to leave us a review of your product.',
                                                                                                                                                         'woocommerce'), $order); ?></p>
							
							<ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">
								
								<li class="iv-wp-from-right woocommerce-order-overview__order order">
                  <?php _e('Order number:', 'woocommerce'); ?>
									<strong><?php echo $order->get_order_number(); ?></strong>
								</li>
								
								<li class="iv-wp-from-left woocommerce-order-overview__date date">
                  <?php _e('Date:', 'woocommerce'); ?>
									<strong><?php echo wc_format_datetime($order->get_date_created()); ?></strong>
								</li>
                
                <?php if (is_user_logged_in() && $order->get_user_id() === get_current_user_id() && $order->get_billing_email()) : ?>
									<li class="iv-wp-from-right woocommerce-order-overview__email email">
                    <?php _e('Email:', 'woocommerce'); ?>
										<strong style="word-break: break-all;"><?php echo $order->get_billing_email(); ?></strong>
									</li>
                <?php endif; ?>
								
								<li class="iv-wp-from-left woocommerce-order-overview__total total">
                  <?php _e('Total:', 'woocommerce'); ?>
									<strong><?php echo $order->get_formatted_order_total(); ?></strong>
								</li>
                
                <?php if ($order->get_payment_method_title()) : ?>
									<li class="iv-wp-from-right woocommerce-order-overview__payment-method method">
                    <?php _e('Payment method:', 'woocommerce'); ?>
										<strong><?php echo wp_kses_post($order->get_payment_method_title()); ?></strong>
									</li>
                <?php endif; ?>
							
							</ul>
            
            <?php endif; ?>
						<div class="iv-wp-from-bottom">
              <?php do_action('woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id()); ?>
						</div>
            
            <?php
            // get order details and billing details from woocommerce/order/order-details.php
            do_action('woocommerce_thankyou', $order->get_id());
            ?>
						<a href="<?php echo esc_url(wc_get_page_permalink('shop')); ?>" class="button">
							<button class="continue oz99-secondary-color oz99-off-white-background-color hover-arrow"><?php _e('Continue Shopping', 'woocommerce') ?>
								<i class="fal fa-long-arrow-right oz99-secondary-color"></i>
							</button>
						</a>
          
          
          <?php else : ?>
						<h2 class="thanks-logo oz99-primary-color">THANK YOU FOR YOUR ORDER!</h2>
						
						<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters('woocommerce_thankyou_order_received_text',
                                                                                                                                   __('Your order has been received. Be sure to leave us a review of your product.',
                                                                                                                                      'woocommerce'), null); ?></p>
						<a href="<?php echo esc_url(wc_get_page_permalink('shop')); ?>" class="button">
							<button class="continue oz99-secondary-color oz99-off-white-background-color hover-arrow"><?php _e('Continue Shopping', 'woocommerce') ?>
								<i class="fal fa-long-arrow-right oz99-secondary-color"></i>
							</button>
						</a>
          
          <?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
