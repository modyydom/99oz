<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if (!defined('ABSPATH')) {
  exit;
}

?>
<div class="checkout-page-wrapper">
	<div class="container">
		<div class="row checkout-page-row">
      
      <?php
      do_action('woocommerce_before_checkout_form', $checkout);
      
      // If checkout registration is disabled and not logged in, the user cannot checkout.
      if (!$checkout->is_registration_enabled() && $checkout->is_registration_required() && !is_user_logged_in()) {
        echo esc_html(apply_filters('woocommerce_checkout_must_be_logged_in_message', __('You must be logged in to checkout.', 'woocommerce')));
        return;
      }
      
      ?>
			<div class="col-lg-8 col-md-12 col-12">
				<div class="checkout-wizard-wrapper">
					<h1 class="page-title oz99-black-color iv-wp-from-left">Checkout</h1>
					<ul class="wizard-nav iv-wp-from-left">
						<li class="wizard-item visited oz99-secondary-color">Cart ></li>
						<li class="wizard-item oz99-black-color active" data-number="1">Information ></li>
						<!--						<li class="wizard-item oz99-black-color valid" data-number="2">Shipping ></li>-->
						<li class="wizard-item oz99-black-color" data-number="3">Payment</li>
					</ul>
					
					<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url(wc_get_checkout_url()); ?>" enctype="multipart/form-data">
            
            <?php if ($checkout->get_checkout_fields()) : ?>
              
              <?php do_action('woocommerce_checkout_before_customer_details'); ?>
	            <input id="confirm-order-flag" name="confirm-order-flag" type="hidden" value="1">
							<div class="information-step active" data-number="1">
								<div class="express-checkout iv-wp-from-bottom">
									<h5 class="the-headline oz99-black-color">EXPRESS CHECKOUT</h5>
									<a class="the-item" href="#">
										<img alt="applepay" src="<?=get_template_directory_uri()?>/assets/images/checkout/applepay.png">
									</a>
									<a class="the-item" href="#">
										<img alt="paypal" src="<?=get_template_directory_uri()?>/assets/images/checkout/paypal.png">
									</a>
								</div>
								<div class="separator iv-wp-from-bottom"><span class="text oz99-black-color">OR</span></div>
                <?php /*
								<h3 class="checkout-subtitle iv-wp-from-left contact-info-subtitle oz99-black-color">CONTACT INFORMATION</h3>
								
								<div class="input-wrapper iv-wp-from-bottom ">
									<label class="oz99-black-color">Email</label>
									<input placeholder="janedoe@gmail.com" type="text">
								</div>
								
								<div class="iv-wp-from-bottom custom-checkbox">
									<label class="checkbox-container">
										Keep me up to date on news and exclusive offers
										<input checked="checked" type="checkbox">
										<span class="checkmark">
											<i class="after fas fa-check"></i>
										</span>
									</label>
								</div>
                */ ?>
                
                <?php do_action('woocommerce_checkout_billing'); ?>
								
								<div class="input-wrapper iv-wp-from-bottom ">
									<button type="submit" class="form-next-step"><?php esc_html_e('CONTINUE TO SHIPPING METHOD', 'woocommerce') ?></button>
								</div>
							
							</div>
							<div class="information-step " data-number="3">
                <?php do_action('woocommerce_checkout_shipping'); ?>
								
								<div class="form-row place-order">
									<noscript>
                    <?php
                    /* translators: $1 and $2 opening and closing emphasis tags respectively */
                    printf(esc_html__('Since your browser does not support JavaScript, or it is disabled, please ensure you click the %1$sUpdate Totals%2$s button before placing your order. You may be charged more than the amount stated above if you fail to do so.',
                                      'woocommerce'), '<em>', '</em>');
                    ?>
										<br/>
										<button type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="<?php esc_attr_e('Update totals', 'woocommerce'); ?>"><?php esc_html_e('Update totals',
                                                                                                                                                                                     'woocommerce'); ?></button>
									</noscript>
                  
                  <?php wc_get_template('checkout/terms.php'); ?>
                  
                  <?php do_action('woocommerce_review_order_before_submit'); ?>
									<div class="input-wrapper iv-wp-from-bottom ">
                    <?php echo apply_filters('woocommerce_order_button_html', '<button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="' .
                                                                            esc_attr(__("COMPLETE ORDER", "woocommerce")) . '" data-value="' . esc_attr(__("COMPLETE ORDER", "woocommerce")) . '">' .
                                                                            esc_html(__("COMPLETE ORDER", "woocommerce")) . '</button>'); // @codingStandardsIgnoreLine ?>
									</div>
                  
                  <?php do_action('woocommerce_review_order_after_submit'); ?>
                  
                  <?php wp_nonce_field('woocommerce-process_checkout', 'woocommerce-process-checkout-nonce'); ?>
								</div>
							
							
							</div>
              <?php do_action('woocommerce_checkout_after_customer_details'); ?>
            
            <?php endif; ?>
					
					
					</form>
          
          <?php do_action('woocommerce_after_checkout_form', $checkout); ?>
				
				</div>
			</div>
			<div class="col-lg-4 col-md-12 col-12">
        
        <?php do_action('woocommerce_checkout_before_order_review_heading'); ?>
				
				<h3 class="order-summary-title oz99-black-color iv-wp-from-right" id="order_review_heading">&nbsp;</h3>
        
        <?php do_action('woocommerce_checkout_before_order_review'); ?>
				
				<div id="order_review" class="woocommerce-checkout-review-order order-summary">
          <?php do_action('woocommerce_checkout_order_review'); ?>
				</div>
        
        <?php do_action('woocommerce_checkout_after_order_review'); ?>
			</div>
		</div>
	</div>
</div>
