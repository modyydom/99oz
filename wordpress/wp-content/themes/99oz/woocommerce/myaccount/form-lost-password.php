<?php
/**
 * Lost password form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-lost-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.2
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_lost_password_form' );
?>
	<div class="container  iv-wp-from-bottom">
		<div class="row" id="customer_login">
			<div class="col-12">
				
				<h2 class="form-title"><?php esc_html_e( 'LOST YOUR PASSWORD?', 'woocommerce' ); ?></h2>
<form method="post" class="woocommerce-form woocommerce-ResetPassword lost_reset_password">

	<?php // @codingStandardsIgnoreLine ?>

	<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
		<label for="user_login"><?php esc_html_e( 'Username or email', 'woocommerce' ); ?></label>
		<input class="woocommerce-Input woocommerce-Input--text input-text" type="text" name="user_login" id="user_login" autocomplete="username" />
	</p>
	<p class="custom-text-wrapper"><?php echo apply_filters( 'woocommerce_lost_password_message', esc_html__( 'Lost your password? Please enter your username or email address. You will receive a link to create a new password via email.', 'woocommerce' ) ); ?></p>
	<div class="clear"></div>

	<?php do_action( 'woocommerce_lostpassword_form' ); ?>

		<input type="hidden" name="wc_reset_password" value="true" />
		<button type="submit" class="hover-arrow okanagan-btn dark-w-arrow woocommerce-Button button" value="<?php esc_attr_e( 'Reset password', 'woocommerce' ); ?>"><?php esc_html_e( 'Reset password', 'woocommerce' ); ?> <i class="fal fa-long-arrow-right"></i></button>

	<?php wp_nonce_field( 'lost_password', 'woocommerce-lost-password-nonce' ); ?>
	<p class="dont-have-account">Already have an account? <a href="<?php echo site_url(); ?>/my-account">Login</a></p>
</form>
	</div>
	</div>
	</div>
<?php
do_action( 'woocommerce_after_lost_password_form' );
?>

<div class="container separator end-of-page thick"></div>
