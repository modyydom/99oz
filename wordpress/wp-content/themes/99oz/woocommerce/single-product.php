<?php
/**
 * The single-product template file
 *
 * @package Okanagan
 */

get_header();
while (have_posts()) : the_post();
    global $product;
    $product_gallery_images_ids = $product->get_gallery_image_ids();

    /**
     * Hook: woocommerce_before_single_product.
     *
     * @hooked wc_print_notices - 10
     */
    do_action('woocommerce_before_single_product');
    ?>
    <div class="single-product-page-wrapper">

        <?php
        if (have_rows('product_page_builder')):
            // loop through the rows of data
            while (have_rows('product_page_builder')) :the_row();
                switch (get_row_layout()):
                    case 'main_product_section':
                        ?>

                        <section class="product-main">
                            <div class="container">
                                <div class="row align-items-center">
                                    <div class="col-12 col-md-7 col-lg-6">
                                        <div class="photos iv-wp-from-left">
                                            <?php foreach ($product_gallery_images_ids as $attachment_id): ?>
                                                <div class="photo">
                                                    <img class="no-zoom"
                                                         src="<?= $image_link = wp_get_attachment_url($attachment_id); ?>">
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-5 col-lg-6">
                                        <div class="main-details" id="product-<?php the_ID(); ?>">
                                            <?php wc_get_template_part('content', 'single-product'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <?php
                        break;
                    case 'info_product_section':
                        ?>

                        <section class="product-details">
                            <div class="container">
                                <div class="row">
                                    <?php while (have_rows('information')): the_row(); ?>
                                        <?php switch (get_sub_field('info_type')):
                                            case 'text': ?>
                                                <div class="col-12 col-md-6 col-lg-4">
                                                    <div class="title oz99-secondary-color iv-wp-from-top"><?php esc_html_e(get_sub_field('title')) ?></div>
                                                    <div class="content oz99-black-color iv-wp"><?php the_sub_field('text') ?></div>
                                                </div>

                                                <?php break;

                                            case 'attribute':
                                                $terms = get_the_terms($product->get_id(), 'pa_' . get_sub_field('attribute'));
                                                ?>

                                                <div class="col-12 col-md-6 col-lg-2">
                                                    <div class="title oz99-secondary-color iv-wp-from-top"><?php esc_html_e(get_sub_field('title')) ?></div>
                                                    <div class="content oz99-black-color">
                                                        <div class="filter-checkboxes">
                                                            <?php foreach ($terms as $term): ?>
                                                                <div class="choice oz-checkbox-group iv-wp-from-bottom">
                                                                    <input type="checkbox" disabled checked>
                                                                    <label for="use1"><?php esc_html_e($term->name) ?></label>
                                                                </div>
                                                            <?php endforeach; ?>
                                                        </div>
                                                    </div>
                                                </div>

                                                <?php break;

                                            case 'checkbox':
                                                ?>
                                                <div class="col-12 col-md-6 col-lg-2">
                                                    <div class="title oz99-secondary-color iv-wp-from-top"><?php esc_html_e(get_sub_field('title')) ?></div>
                                                    <div class="content oz99-black-color">
                                                        <div class="filter-checkboxes">
                                                            <?php foreach (get_sub_field('checkbox') as $item): ?>
                                                                <div class="choice oz-checkbox-group iv-wp-from-bottom">
                                                                    <input type="checkbox" disabled checked>
                                                                    <label for="use1"><?php esc_html_e($item) ?></label>
                                                                </div>
                                                            <?php endforeach; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php break;
                                        endswitch;
                                    endwhile; ?>
                                </div>
                                <hr class="oz99-black-background-color iv-wp-from-top">
                            </div>
                        </section>

                        <?php
                        break;
                    case 'customer_reviews_section':
                        ?>

                        <section class="reviews">
                            <div class="container">
                                <div class="row justify-content-between">
                                    <div class="col-auto">
                                        <h2 class="section-header oz99-black-color iv-wp-from-top"><?php esc_html_e(get_sub_field('section_title')) ?></h2>
                                        <?php woocommerce_template_single_rating() ?>
                                    </div>
                                    <div class="col-auto">
                                        <button class="iv-wp-from-right"><?php esc_html_e(get_sub_field('btn_text')) ?></button>
                                    </div>
                                </div>
                                <div class="row reviews-container">
                                <?php CommentsQuery(); ?>
                        </section>
                        <div class="container">
                                <hr class="oz99-black-background-color iv-wp-from-top">
                        </div>
                        <!-- Add Review System -->
  <div id="review_form_wrapper">
							<div class="container">
								<div id="review_form">
                    <div class="row">
                        <div class="col-md-8">
                          <?php
                          $commenter = wp_get_current_commenter();

        $comment_form = array( 'title_reply'         => sprintf('LEAVE YOUR REVIEW FOR "%s"', get_the_title()),
                                'title_reply_to'      => __('Leave a Reply to %s', 'woocommerce'),
                                'title_reply_before'  => '<span id="reply-title" class="comment-reply-title">',
                                'title_reply_after'   => '</span><p>Your email address will not be published. Required fields are marked *</p>',
                                'comment_notes_after' => '',
                                'fields'              => array('author' => '<p class="comment-form-author">'.'<label for="author">'.esc_html__('Name', 'woocommerce').'&nbsp;    <span class="required">*</span></label> '.'<input id="author" name="author" type="text" value="'.esc_attr($commenter['comment_author']).'"     size="30" aria-required="true" required /></p>',
                                                              'email'  => '<p class="comment-form-email"><label for="email">'.esc_html__('Email', 'woocommerce').'&nbsp;<span     class="required">*</span></label> '.'<input id="email" name="email" type="email" value="'.esc_attr($commenter['comment_author_email']).'"     size="30" aria-required="true" required /></p>',),
                                'label_submit'        => __('Submit', 'woocommerce'),
                                'logged_in_as'        => '',
                                'comment_field'       => '<p class="comment-form-comment"><label for="comment">' . _x( 'Comment', 'noun' ) . '</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>',
                                'submit_button'      => '<button type="submit" name="%1$s" id="%2$s" class="%3$s button dark-w-arrow okanagan-btn">Submit<i class="the-icon fal fa-long-arrow-right"></i></button>'
        );

                if ($account_page_url = wc_get_page_permalink('myaccount')) {
                  $comment_form['must_log_in'] = '<p class="must-log-in">'.sprintf(__('You must be <a href="%s">logged in</a> to post a     review.', 'woocommerce'), esc_url($account_page_url)).'</p>';
                }

                if (get_option('woocommerce_enable_review_rating') === 'yes') {
                  $comment_form['comment_field'] = '
                          <div class="comment-form-rating">
                              <h3>YOUR RATING</h3>
                               <div class="rating">
                            <input type="radio" id="star5" name="rating" value="5" />
                            <label for="star5" title="text">5 stars</label>
                            <input type="radio" id="star4" name="rating" value="4" />
                            <label for="star4" title="text">4 stars</label>
                            <input type="radio" id="star3" name="rating" value="3" />
                            <label for="star3" title="text">3 stars</label>
                            <input type="radio" id="star2" name="rating" value="2" />
                            <label for="star2" title="text">2 stars</label>
                            <input type="radio" id="star1" name="rating" value="1" />
                            <label for="star1" title="text">1 star</label>
                          </div>
                              <p class="title-for-comment"><label for="title">Title</label><input type="text" name="title" id="title" required></p>';
                }

                $comment_form['comment_field'] .= '<p class="comment-form-comment"><label for="comment">'.esc_html__('Your review', 'woocommerce').'&nbsp;<span class="required">*</span></label><textarea id="comment" name="comment" cols="45" rows="8"     aria-required="true" required></textarea></p>';

                comment_form(apply_filters('woocommerce_product_review_comment_form_args', $comment_form));
                ?>
              </div>
                    </div>
								</div>
							</div>
						</div>


                          <!-- END Review System -->

                        <div class="container">
                        <hr class="iv-wp-from-top">
                        </div>
                        <?php
                        break;
                    case 'other_products_section':
                        ?>
                        <section class="product-big-slider">
                            <div class="container">
                                <h1 class="section-header-other"><?php the_sub_field('other_products_section_title') ?></h1>
                                <?php
                                $args = array('post_type' => 'product',
                                    'posts_per_page' => -1);
                                $loop = new WP_Query($args);
                                if ($loop->have_posts()) {
                                    $data = array('images' => [],
                                        'description' => []);
                                    ?>
                                    <div class="row align-items-center justify-content-center">

                                        <?php
                                        ob_start();
                                        while ($loop->have_posts()) : $loop->the_post();
                                            array_push($data['images'], get_the_post_thumbnail_url(get_the_ID()));
                                            //                    wc_get_product( id )
                                            ?>

                                            <div class="main-details" id="product-<?php the_ID(); ?>">
                                                <?php wc_get_template_part('content', 'single-product'); ?>
                                            </div>

                                            <?php
                                            array_push($data['description'], ob_get_contents());
                                            ob_clean();
                                        endwhile;
                                        ob_end_clean();
                                        ?>
                                        <div class="col-12 col-md-5 changing-position">
                                            <div class="photo">
                                                <div class="middle-image-container">
                                                    <?php foreach ($data['images'] as $image) : ?>
                                                        <img src="<?= esc_url($image) ?>" class="middle-image no-zoom"
                                                             alt="">
                                                    <?php endforeach; ?>
                                                </div>
                                                <?php
                                                foreach ($data['images'] as $image) {
                                                    ?>
                                                    <div class="left-slide">
                                                        <img src="<?= esc_url($image) ?>" class="no-zoom" alt="">
                                                    </div>
                                                    <div class="right-slide">
                                                        <img src="<?= esc_url($image) ?>" class="no-zoom" alt="">
                                                    </div>
                                                    <?php
                                                } ?>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6 offset-md-1">
                                            <?php foreach ($data['description'] as $description) {
                                                echo $description;
                                            } ?>
                                        </div>
                                    </div>
                                <?php } else {
                                    /**
                                     * Hook: woocommerce_no_products_found.
                                     *
                                     * @hooked wc_no_products_found - 10
                                     */
                                    do_action('woocommerce_no_products_found');
                                }


                                wp_reset_postdata();

                                ?>

                            </div>
                        </section>
                        <?php
                        break;

                endswitch;
                /*
                if (get_row_layout() == 'main_product_section') { ?>
                  <!-- region Product overview section -->
                  <section class="product-overview-section">
                    <div class="container">
                      <div class="separator first iv-wp-from-top thick"></div>
                      <div class="row justify-content-center">
                        <div class="col-12 col-lg-6 order-last order-lg-first">
                          <div class="product-gallery iv-wp-from-top">
                            <div class="big-image iv-wp-from-top" draggable="false">
                              <div class="img-container zoom" draggable="false">
                                <img class="no-zoom" src="
                                <?php if ($product_gallery_images_ids && $product_gallery_images_ids[0]) {
                                  echo wp_get_attachment_url($product_gallery_images_ids[0]);
                                } else {
                                  the_post_thumbnail_url();
                                } ?>" alt="" draggable="false">
                                <h3 class="d-block d-lg-none" draggable="false">
                                  <i class="fal fa-hand-pointer"></i>
                                  Grab To Zoom
                                </h3>
                                <h3 class="d-none d-lg-block" draggable="false">
                                  <i class="fal fa-mouse-pointer"></i>
                                  Hover To Zoom
                                </h3>
                              </div>
                            </div>
                            <div class="small-images iv-wp-from-bottom">
                              <?php
                              foreach ($product_gallery_images_ids as $attachment_id) {
                                ?>
                                <div class="img-wrapper">
                                  <img src="<?=$image_link = wp_get_attachment_url($attachment_id);?>" alt="">
                                </div>
                                <?php
                              } ?>
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-6 col-12 iv-wp-from-bottom" style="position: relative;">
                          <div class="zooming-area" style="pointer-events:none;position: absolute;z-index: 10;top: 0;left: 0;width: 100%;height: 100%;"></div>
                          <div class="product-content">
                            <?php wc_get_template_part('content', 'single-product'); ?>


                            <!--							<div class="product-specifications">-->
                            <!--								<div class="specification iv-wp-from-right"><span>///</span> no THC<br></div>-->
                            <!--								<div class="specification iv-wp-from-right"><span>///</span> 1000mg<br></div>-->
                            <!--								<div class="specification iv-wp-from-right"><span>///</span> Full Spectrum: CBD, THC, CBG, CBN, and CBC<br></div>-->
                            <!--								<div class="specification iv-wp-from-right"><span>///</span> Non-GMO<br></div>-->
                            <!--								<div class="specification iv-wp-from-right"><span>///</span> Made from 100% Organic Canadian-grown Hemp<br></div>-->
                            <!--							</div>-->
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                  <!-- endregion Product overview section -->
                <?php }
                if (get_row_layout() == 'info_product_section') {
                  $info_tab_background = get_sub_field('info_tab_background');
                  ?>
                  <!--region Product Faqs section -->
                  <section class="product-faqs-section" style='background-image: url("<?php echo $info_tab_background; ?>")'>
                    <!-- region Page questions -->
                    <div class="page-questions">
                      <div class="container">
                        <?php
                        if (have_rows('info_tabs')):
                          $counter = 0;
                          while (have_rows('info_tabs')) : the_row();
                            $info_tab_title = get_sub_field('info_tab_title');
                            ?>

                            <div class="questions-group iv-wp-from-bottom <?php if ($counter == 0) {
                              echo 'active toggled';
                            } ?>">
                              <div class="the-big-header toggleable-slide-up-header toggleable-slide-up-header-opened">
                                <i class="fal fa-plus"></i>
                                <i class="fal fa-minus"></i>
                                <h4><?php echo $info_tab_title; ?></h4>
                              </div>
                              <div class="the-big-body toggleable-slide-up-body">
                                <div class="row">
                                  <?php
                                  if (have_rows('info_tab_content')):
                                    while (have_rows('info_tab_content')) : the_row();
                                      $content_headline = get_sub_field('content_headline');
                                      $content_body = get_sub_field('content_body');
                                      ?>
                                      <div class="col-12 col-md-6 col-lg-4">
                                        <div class="the-text-wrapper">
                                          <div class="the-header"><?php echo $content_headline; ?></div>
                                          <div class="the-body">
                                            <div class="text">
                                              <?php echo $content_body; ?>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    <?php endwhile;
                                  endif;
                                  ?>

                                </div>
                              </div>
                            </div>

                            <?php $counter++; endwhile;
                        endif;
                        ?>
                      </div>
                    </div>
                    <!-- endregion Page questions -->
                  </section>
                  <!--endregion Product Faqs section -->
                <?php }
                if (get_row_layout() == 'benefits_product_section') { ?>
                  <!--region Benefits section -->
                  <section class="product-benefits-section">
                    <div class="container">
                      <h2 class="section-title iv-wp-from-bottom"><?php echo get_sub_field('section_title'); ?></h2>
                      <div class="separator iv-wp-from-top thick"></div>

                      <div class="product-benefits-wrapper">
                        <div class="product-benefits right">
                          <div class="benefit-wrapper benefit_1 iv-wp-from-right">
                            <img src="<?=get_template_directory_uri()?>/assets/images/single-product/benefit_1.png" alt="">
                            <h4>HEALTH <span class="dot"></span></h4>
                            <p>A short statement here. Lorem ipsum dolor sit amet poenam consectitur Lorem ipsum dolor sit</p>
                          </div>
                          <div class="benefit-wrapper benefit_2 iv-wp-from-right">
                            <img src="<?=get_template_directory_uri()?>/assets/images/single-product/benefit_2.png" alt="">
                            <h4>RECOVER <span class="dot"></span></h4>
                            <p>A short statement here. Lorem ipsum dolor sit amet poenam consectitur</p>
                          </div>
                        </div>

                        <div class="product-image-center iv-wp-from-top">
                          <img src="<?=get_template_directory_uri()?>/assets/images/single-product/single-product.png" alt="">
                        </div>

                        <div class="product-benefits">
                          <div class="benefit-wrapper benefit_3 right-side iv-wp-from-left">
                            <img src="<?=get_template_directory_uri()?>/assets/images/single-product/benefit_3.png" alt="">
                            <h4>MOOD <span class="dot"></span></h4>
                            <p>A short statement here. Lorem ipsum dolor sit amet poenam consectitur ipsum dolor sit amet</p>
                          </div>
                          <div class="benefit-wrapper benefit_4 right-side iv-wp-from-left">
                            <img src="<?=get_template_directory_uri()?>/assets/images/single-product/benefit_4.png" alt="">
                            <h4>RELAX <span class="dot"></span></h4>
                            <p>A short statement here. Lorem ipsum dolor sit amet poenam consectitur ipsum dolor sit amet</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                  <!--endregion Benefits section -->
                <?php }
                if (get_row_layout() == 'customer_reviews_section') { ?>
                  <!--region Customer Reviews section -->
                  <section id="reviews" class="customer-reviews-section">
                    <div class="container">
                      <div class="separator iv-wp-from-top thick"></div>
                      <h2 class="section-title iv-wp-from-left"><?php echo get_sub_field('section_title'); ?></h2>
                      <h5 class="reviews-counter iv-wp-from-left">(<?php echo $product->get_review_count(); ?> review<?php echo $product->get_review_count() > 1 ? 's' : ''; ?>)</h5>

                      <div class="customer-reviews-wrapper">
                        <?php
                        $args = array(// args here
                                      'post_id' => get_the_ID());

                        // The Query
                        $comments_query = new WP_Comment_Query;
                        $comments = $comments_query->query($args);

                        // Comment Loop
                        if ($comments) {
                          foreach ($comments as $comment) {
                            if ($comment->comment_type == 'review') {
                              $rating = get_comment_meta($comment->comment_ID, 'rating', true);
                              ?>

                              <div class="customer-review">
                                <div class="row">
                                  <div class="col-lg-3 col-sm-4 col-12 iv-wp-from-left">
                                    <div class="user">
                                      <div class="profile-img-container">
                                        <?php
                                        $author_email = $comment->comment_author_email;
                                        $get_author_gravatar = 'http://www.gravatar.com/avatar/' . md5(strtolower($author_email)) . '';
                                        ?>
                                        <img src="<?php echo $get_author_gravatar; ?>" alt="woman profile picture">
                                      </div>
                                      <div class="name">
                                        <h4><?php echo $comment->comment_author; ?></h4>
                                        <div class="stars">
                                          <?php display_rating_stars($rating) ?>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="offset-lg-1 col-lg-8 col-sm-8 col-12 iv-wp-from-right">
                                    <div class="review-content">
                                      <h3><?php the_title(); ?></h3>
                                      <p><?php echo $comment->comment_content ?></p>
                                    </div>
                                  </div>
                                </div>
                              </div>

                            <?php }
                          }
                        } else {
                          echo 'No comments found.';
                        }
                        ?>

                      </div>

                      <div class="separator end-of-page thick"></div>
                    </div>
                  </section>
                  <!--endregion Customer Reviews section -->
                <?php }
                */
            endwhile;

        else:
            echo "please setup the product from the dashboard first";
        endif;
        ?>
    </div>

<?php endwhile; // end of the loop. ?>
<?php
get_footer();
