<!-- Begin Sidebar -->
<form action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" id="filter">
    <div class="prices-filter-sidebar">
        <div class="sidebar-title oz99-secondary-color iv-wp-from-top">
            PRICE
        </div>
        <div class="prices-boxes iv-wp-from-left">
            <?php getPricesAttributes(); ?>
        </div>

        <div class="divider"></div>
        <div class="section">
            <div class="sidebar-title oz99-secondary-color iv-wp-from-top">
                TYPE
            </div>
            <div class="row types-choises iv-wp-from-left">
                <?php getTypesAttributes(); ?>
            </div>
        </div>
        <div class="divider"></div>
        <div class="section">
            <div class="sidebar-title oz99-secondary-color iv-wp-from-top">
                EFFECTS
            </div>
            <div class="checkboxes effects-filter-checkboxes iv-wp-from-left">
                <!-- That's how we get Effects items for sidebar -->
                <?php getEffectsAttributes(); ?>
                <!-- End of text! -->
            </div>
        </div>
        <div class="divider"></div>
        <div class="section">
            <div class="sidebar-title oz99-secondary-color iv-wp-from-top">
                USAGES
            </div>
            <div class="checkboxes usages-filter-checkboxes iv-wp-from-left">
                <!-- Guess what? We'll get usages items below -->
                <?php getUsagesAttributes(); ?>
                <!-- unfortunatly.. End of text! -->
            </div>
        </div>
        <div class="divider" style="margin-bottom:0px"></div>
        <button name="reset" id="reset-filter" type="reset" class="reset-btn">Clear Filters</button>
        <div class="divider reset-btn-divider" style="margin-bottom:0px"></div>

        <!-- End of Sidebar -->
    </div>
    <button id="apply-filter-btn">Apply filter</button>
    <input type="hidden" name="action" value="myfilter">
</form>