<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Okanagan
 */

get_header();
?>
  <div class="home-page-wrapper">
    <section class="hero">
      <img class="hero-bg" src="<?=get_template_directory_uri()?>/assets/images/home/home-header-bg.png" alt="background image of forest">
      <div class="content">
        <div class="text-container">
          <h4 class="small-title iv-wp">CANADA’S FINEST CBD OIL & POWDERS</h4>
          <h2 class="title iv-wp">A Quality
            You Can Feel</h2>
          <p class="description iv-wp">Sourced exclusively from North American, certified organic hemp farms.</p>
          <button class="iv-wp-from-bottom hover-arrow">SHOP <i class="fal fa-long-arrow-right"></i></button>
        </div>
        <div class="img-container iv-wp-from-right">
          <img src="<?=get_template_directory_uri()?>/assets/images/home/home-products-header-img.png" alt="products group image" class="header-img">
        </div>
      </div>
    </section>
    
    <section class="why-love-us">
      <div class="container">
        <div class="top-criteria-links row">
          <div class="iv-wp-from-right col-12 col-sm-6 col-lg-3">
            <div class="criterion colored-top-border brown3 hover-arrow">
              <h3>NATURAL INGREDIENTS<i class="fal fa-long-arrow-right"></i></h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing</p>
            </div>
          </div>
          <div class="iv-wp-from-left col-12 col-sm-6 col-lg-3">
            <div class="criterion colored-top-border brown1 hover-arrow">
              <h3>FULL SPECTRUM CBD<i class="fal fa-long-arrow-right"></i></h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing</p>
            </div>
          </div>
          <div class="iv-wp-from-right col-12 col-sm-6 col-lg-3">
            <div class="criterion colored-top-border brown2 hover-arrow">
              <h3>EASY TO USE<i class="fal fa-long-arrow-right"></i></h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing</p>
            </div>
          </div>
          <div class="iv-wp-from-left col-12 col-sm-6 col-lg-3">
            <div class="criterion colored-top-border brown4 hover-arrow">
              <h3>EASE TO MICRODOSE<i class="fal fa-long-arrow-right"></i></h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing</p>
            </div>
          </div>
        </div>
        <div class="main row">
          <div class="iv-wp-from-left col-12 col-md-5">
            <img src="<?=get_template_directory_uri()?>/assets/images/home/home-why-us-img.png" alt="products group image" class="why-love-us-img">
          </div>
          <div class="col-12 col-md-7">
            <div class="why-love-us-description hover-arrow">
              <h2 class="iv-wp">Why You’ll Love <span style="display: inline-block;">Us<i class="fal fa-long-arrow-right"></i></span></h2>
              <p class="iv-wp">This is where product introduction will be displayed. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="our-products">
      <div class="container">
        <div class="separator iv-wp-from-top">
          <div class="text">OUR PRODUCTS</div>
        </div>
        
        <div class="row align-items-stretch justify-content-around align-content-stretch">
          <div class="col-12 col-sm-8 col-md-6 col-lg-4">
            <div class="product hover-arrow">
              <img src="<?=get_template_directory_uri()?>/assets/images/home/home-products-bg-1.png" alt="white fabric image" class="product-bg iv-wp-from-bottom fabric">
              <img src="<?=get_template_directory_uri()?>/assets/images/home/home-products-img-1.png" alt="BROAD SPECTRUM CBD OIL" class="product-img iv-wp-from-top">
              <h3 class="product-name iv-wp">
                BROAD SPECTRUM<br>CBD OIL<br>
                <i class="fal fa-long-arrow-right"></i>
              </h3>
            </div>
          </div>
          <div class="col-12 col-sm-8 col-md-6 col-lg-4">
            <div class="product hover-arrow">
              <img src="<?=get_template_directory_uri()?>/assets/images/home/home-products-bg-2.png" alt="two branches picture" class="product-bg iv-wp-from-bottom branches">
              <img src="<?=get_template_directory_uri()?>/assets/images/home/home-products-img-2.png" alt="CBD ISOLATE" class="product-img iv-wp-from-top large">
              <h3 class="product-name iv-wp">
                CBD<br>ISOLATE<br>
                <i class="fal fa-long-arrow-right"></i>
              </h3>
            </div>
          </div>
          <div class="col-12 col-sm-8 col-md-6 col-lg-4">
            <div class="product hover-arrow">
              <img src="<?=get_template_directory_uri()?>/assets/images/home/home-products-bg-3.png" alt="picture of mouse, stick and ball" class="product-bg iv-wp-from-bottom stick">
              <img src="<?=get_template_directory_uri()?>/assets/images/home/home-products-img-3.png" alt="PET CBD OIL" class="product-img iv-wp-from-top">
              <h3 class="product-name iv-wp">
                PET<br>CBD OIL<br>
                <i class="fal fa-long-arrow-right"></i>
              </h3>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section style="height: 40px;"></section>
    
    <section class="about-us">
      <div class="container">
        <div class="separator iv-wp-from-top">
          <div class="text">MORE ABOUT CBD</div>
        </div>
        <div class="row align-items-center justify-content-around">
          <div class="col-12 col-sm-8 col-md-6 col-lg-4">
            <div class="top-card hover-arrow iv-wp-from-top cbd-card no-img colored-top-border brown3">
              <div class="card-bottom">
                <div class="text-container">
                  <h3 class="iv-wp">99.9% CBD</h3>
                  <p class="iv-wp">Simply put isolate is the purest form of CBD. There is 1000 mg of CBD in 1 gram of isolate...</p>
                </div>
                <i class="fal fa-long-arrow-right iv-wp-from-top"></i>
              </div>
            </div>
            <div class="bottom-card hover-arrow iv-wp-from-bottom">
              <img src="<?=get_template_directory_uri()?>/assets/images/home/home-more-about-img-1.png" alt="CBD powder picture" class="bg">
              <div class="icons iv-wp-from-bottom">
                <i class="fal fa-tint"></i>
                <i class="fal fa-tint"></i>
                <i class="fal fa-tint"></i>
              </div>
              <h5 class="iv-wp">LEARN ABOUT</h5>
              <h3 class="iv-wp">
                OKANAGAN <br> CBD ISOLATE
                <i class="fal fa-long-arrow-right iv-wp-from-top"></i>
              </h3>
            </div>
          </div>
          <div class="col-12 col-sm-8 col-md-6 col-lg-4">
            <div class="top-card hover-arrow iv-wp-from-top cbd-card no-img colored-top-border brown3">
              <div class="card-bottom">
                <div class="text-container">
                  <h3 class="iv-wp">EASY TO USE</h3>
                  <p class="iv-wp">Cbd isolate has no taste or smell to it and is very discreet...</p>
                </div>
                <i class="fal fa-long-arrow-right iv-wp-from-top"></i>
              </div>
            </div>
            <div class="bottom-card hover-arrow iv-wp-from-bottom">
              <img src="<?=get_template_directory_uri()?>/assets/images/home/home-more-about-img-2.png" alt="picture of woman on water" class="bg">
              <div class="icons"><i class="fal fa-heartbeat"></i></div>
              <h5 class="iv-wp">LEARN ABOUT</h5>
              <h3 class="iv-wp">
                BENEFITS <br> OF CBD
                <i class="fal fa-long-arrow-right"></i>
              </h3>
            </div>
          </div>
          <div class="col-12 col-sm-8 col-md-6 col-lg-4">
            <div class="top-card hover-arrow iv-wp-from-top cbd-card no-img colored-top-border brown3">
              <div class="card-bottom">
                <div class="text-container">
                  <h3 class="iv-wp">MAKE YOUR OWN</h3>
                  <p class="iv-wp">Are you interested in creating your own line of CBD products? CBD isolate is what you need.</p>
                </div>
                <i class="fal fa-long-arrow-right"></i>
              </div>
            </div>
            <div class="bottom-card hover-arrow iv-wp-from-bottom">
              <img src="<?=get_template_directory_uri()?>/assets/images/home/home-more-about-img-3.png" alt="CBD oil bottle" class="bg">
              <div class="icons">
                <img src="<?=get_template_directory_uri()?>/assets/images/home/home-bottle-icon.png" alt="bottle icon">
              </div>
              <h5 class="iv-wp">TRY IT</h5>
              <h3 class="iv-wp">
                MAKE <br> YOUR OWN
                <i class="fal fa-long-arrow-right"></i>
              </h3>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="wholesale-inquiry">
      <!--      <img src="./assets/images/home/!@#!##$%^%^%&^.png" alt="">-->
      <h2 class="iv-wp">Wholesale Prices Available!</h2>
      <p class="iv-wp">Are you a business or company looking for larger quantities?
        No problem, we have the most competitive
        wholesale prices in Canada.</p>
      
      <button class="iv-wp-from-bottom hover-arrow">
        WHOLESALE INQUIRY
        <i class="fal fa-long-arrow-right"></i>
      </button>
    
    </section>
    
    <section class="instagram-photos iv-wp-from-top">
      <div class="slide">
        <img src="<?=get_template_directory_uri()?>/assets/images/home/home-footer-img-1.png" alt="">
      </div>
      <div class="slide">
        <img src="<?=get_template_directory_uri()?>/assets/images/home/home-footer-img-2.png" alt="">
      </div>
      <div class="slide">
        <img src="<?=get_template_directory_uri()?>/assets/images/home/home-footer-img-3.png" alt="">
      </div>
      <div class="slide">
        <img src="<?=get_template_directory_uri()?>/assets/images/home/home-footer-img-1.png" alt="">
      </div>
      <div class="slide">
        <img src="<?=get_template_directory_uri()?>/assets/images/home/home-footer-img-2.png" alt="">
      </div>
      <div class="slide">
        <img src="<?=get_template_directory_uri()?>/assets/images/home/home-footer-img-3.png" alt="">
      </div>
      <div class="slide">
        <img src="<?=get_template_directory_uri()?>/assets/images/home/home-footer-img-1.png" alt="">
      </div>
      <div class="slide">
        <img src="<?=get_template_directory_uri()?>/assets/images/home/home-footer-img-2.png" alt="">
      </div>
      <div class="slide">
        <img src="<?=get_template_directory_uri()?>/assets/images/home/home-footer-img-3.png" alt="">
      </div>
      <div class="slide">
        <img src="<?=get_template_directory_uri()?>/assets/images/home/home-footer-img-1.png" alt="">
      </div>
      <div class="slide">
        <img src="<?=get_template_directory_uri()?>/assets/images/home/home-footer-img-2.png" alt="">
      </div>
      <div class="slide">
        <img src="<?=get_template_directory_uri()?>/assets/images/home/home-footer-img-3.png" alt="">
      </div>
    </section>
    
    <section class="testimonials">
      <div class="container">
        <h3 class="title iv-wp">WHAT OUR CLIENTS SAY:</h3>
        <div class="testimonials-slider iv-wp-from-top">
          <div class="testimonial">
            <div class="user">
              <div class="profile-img-container">
                <img src="<?=get_template_directory_uri()?>/assets/images/home/home-testimonials-img.png" alt="woman profile picture">
              </div>
              <div class="name">
                <h4>THCHELSEY</h4>
                <div class="stars">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                </div>
              </div>
            </div>
            <div class="text">Great price for the quality. When dabbing, it is an extremely smooth vapour with a light cherry taste, no harshness like the solvent extracted.</div>
          </div>
          <div class="testimonial">
            <div class="user">
              <div class="profile-img-container">
                <img src="<?=get_template_directory_uri()?>/assets/images/home/home-testimonials-img.png" alt="woman profile picture">
              </div>
              <div class="name">
                <h4>THCHELSEY</h4>
                <div class="stars">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                </div>
              </div>
            </div>
            <div class="text">Great price for the quality. When dabbing, it is an extremely smooth vapour with a light cherry taste, no harshness like the solvent extracted.</div>
          </div>
          <div class="testimonial">
            <div class="user">
              <div class="profile-img-container">
                <img src="<?=get_template_directory_uri()?>/assets/images/home/home-testimonials-img.png" alt="woman profile picture">
              </div>
              <div class="name">
                <h4>THCHELSEY</h4>
                <div class="stars">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                </div>
              </div>
            </div>
            <div class="text">Great price for the quality. When dabbing, it is an extremely smooth vapour with a light cherry taste, no harshness like the solvent extracted.</div>
          </div>
          <div class="testimonial">
            <div class="user">
              <div class="profile-img-container">
                <img src="<?=get_template_directory_uri()?>/assets/images/home/home-testimonials-img.png" alt="woman profile picture">
              </div>
              <div class="name">
                <h4>THCHELSEY</h4>
                <div class="stars">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                </div>
              </div>
            </div>
            <div class="text">Great price for the quality. When dabbing, it is an extremely smooth vapour with a light cherry taste, no harshness like the solvent extracted.</div>
          </div>
          <div class="testimonial">
            <div class="user">
              <div class="profile-img-container">
                <img src="<?=get_template_directory_uri()?>/assets/images/home/home-testimonials-img.png" alt="woman profile picture">
              </div>
              <div class="name">
                <h4>THCHELSEY</h4>
                <div class="stars">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                </div>
              </div>
            </div>
            <div class="text">Great price for the quality. When dabbing, it is an extremely smooth vapour with a light cherry taste, no harshness like the solvent extracted.</div>
          </div>
          <div class="testimonial">
            <div class="user">
              <div class="profile-img-container">
                <img src="<?=get_template_directory_uri()?>/assets/images/home/home-testimonials-img.png" alt="woman profile picture">
              </div>
              <div class="name">
                <h4>THCHELSEY</h4>
                <div class="stars">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                </div>
              </div>
            </div>
            <div class="text">Great price for the quality. When dabbing, it is an extremely smooth vapour with a light cherry taste, no harshness like the solvent extracted.</div>
          </div>
          <div class="testimonial">
            <div class="user">
              <div class="profile-img-container">
                <img src="<?=get_template_directory_uri()?>/assets/images/home/home-testimonials-img.png" alt="woman profile picture">
              </div>
              <div class="name">
                <h4>THCHELSEY</h4>
                <div class="stars">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                </div>
              </div>
            </div>
            <div class="text">Great price for the quality. When dabbing, it is an extremely smooth vapour with a light cherry taste, no harshness like the solvent extracted.</div>
          </div>
          <div class="testimonial">
            <div class="user">
              <div class="profile-img-container">
                <img src="<?=get_template_directory_uri()?>/assets/images/home/home-testimonials-img.png" alt="woman profile picture">
              </div>
              <div class="name">
                <h4>THCHELSEY</h4>
                <div class="stars">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                </div>
              </div>
            </div>
            <div class="text">Great price for the quality. When dabbing, it is an extremely smooth vapour with a light cherry taste, no harshness like the solvent extracted.</div>
          </div>
          <div class="testimonial">
            <div class="user">
              <div class="profile-img-container">
                <img src="<?=get_template_directory_uri()?>/assets/images/home/home-testimonials-img.png" alt="woman profile picture">
              </div>
              <div class="name">
                <h4>THCHELSEY</h4>
                <div class="stars">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                </div>
              </div>
            </div>
            <div class="text">Great price for the quality. When dabbing, it is an extremely smooth vapour with a light cherry taste, no harshness like the solvent extracted.</div>
          </div>
          <div class="testimonial">
            <div class="user">
              <div class="profile-img-container">
                <img src="<?=get_template_directory_uri()?>/assets/images/home/home-testimonials-img.png" alt="woman profile picture">
              </div>
              <div class="name">
                <h4>THCHELSEY</h4>
                <div class="stars">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                </div>
              </div>
            </div>
            <div class="text">Great price for the quality. When dabbing, it is an extremely smooth vapour with a light cherry taste, no harshness like the solvent extracted.</div>
          </div>
          <div class="testimonial">
            <div class="user">
              <div class="profile-img-container">
                <img src="<?=get_template_directory_uri()?>/assets/images/home/home-testimonials-img.png" alt="woman profile picture">
              </div>
              <div class="name">
                <h4>THCHELSEY</h4>
                <div class="stars">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                </div>
              </div>
            </div>
            <div class="text">Great price for the quality. When dabbing, it is an extremely smooth vapour with a light cherry taste, no harshness like the solvent extracted.</div>
          </div>
          <div class="testimonial">
            <div class="user">
              <div class="profile-img-container">
                <img src="<?=get_template_directory_uri()?>/assets/images/home/home-testimonials-img.png" alt="woman profile picture">
              </div>
              <div class="name">
                <h4>THCHELSEY</h4>
                <div class="stars">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                </div>
              </div>
            </div>
            <div class="text">Great price for the quality. When dabbing, it is an extremely smooth vapour with a light cherry taste, no harshness like the solvent extracted.</div>
          </div>
          <div class="testimonial">
            <div class="user">
              <div class="profile-img-container">
                <img src="<?=get_template_directory_uri()?>/assets/images/home/home-testimonials-img.png" alt="woman profile picture">
              </div>
              <div class="name">
                <h4>THCHELSEY</h4>
                <div class="stars">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                </div>
              </div>
            </div>
            <div class="text">Great price for the quality. When dabbing, it is an extremely smooth vapour with a light cherry taste, no harshness like the solvent extracted.</div>
          </div>
          <div class="testimonial">
            <div class="user">
              <div class="profile-img-container">
                <img src="<?=get_template_directory_uri()?>/assets/images/home/home-testimonials-img.png" alt="woman profile picture">
              </div>
              <div class="name">
                <h4>THCHELSEY</h4>
                <div class="stars">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                </div>
              </div>
            </div>
            <div class="text">Great price for the quality. When dabbing, it is an extremely smooth vapour with a light cherry taste, no harshness like the solvent extracted.</div>
          </div>
          <div class="testimonial">
            <div class="user">
              <div class="profile-img-container">
                <img src="<?=get_template_directory_uri()?>/assets/images/home/home-testimonials-img.png" alt="woman profile picture">
              </div>
              <div class="name">
                <h4>THCHELSEY</h4>
                <div class="stars">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                </div>
              </div>
            </div>
            <div class="text">Great price for the quality. When dabbing, it is an extremely smooth vapour with a light cherry taste, no harshness like the solvent extracted.</div>
          </div>
        </div>
      </div>
    </section>
  </div>
<?php
get_footer();
