<?php
/*
  *
  * Template name: Contact Us
  * */

get_header();
$right_block = get_field('contact_us_right_block');
?>
	<div class="contact-us-page-wrapper">
		<!-- region Page Header -->
		<div class="general-page-header">
			<div class="container">
				<div class="row justify-content-md-between justify-content-center">
					<div class="col-11 col-md-6 col-lg-7 col-xl-7">
						<div class="text-with-icon d-flex">
							<div class="overflow-hidden the-icon iv-wp-from-left">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/contact-us/icon.png" alt="Lamb Icon">
							</div>
							<div class="the-text col-10">
								<h4 class="iv-wp-from-top"><?php esc_html_e(get_field('contact_us_title'))?></h4>
								<p class="the-text-width iv-wp">
									<?php esc_html_e(get_field('contact_us_paragraph'))?>
								</p>
							</div>
						</div>
					</div>
					<div class="col-10 col-md-6 col-lg-5 col-xl-5">
						<div class="img-text-card colored-top-border <?=convert_color_to_class($right_block['color'])?> d-flex  iv-wp-from-right">
							<div class="the-image">
								<img src="<?=$right_block['image']['url']?>" alt="<?php esc_attr_e($right_block['image']['atr'])?>">
							</div>
							<div class="the-text">
								<p class="the-text-max-width"><?php esc_html_e($right_block['small_text'])?></p>
								<a href="<?php esc_attr_e($right_block['link'])?>"><?php esc_html_e($right_block['big_text'])?> <i class="fal fa-long-arrow-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- endregion Page Header -->
		<!-- region form contact-us -->
		<section class="contact-form">
			<div class="container">
				<div class="contact-me">
					<div class="content-flex">
						<div class="phone">
							<p class="phone-number iv-wp-from-left">PHONE: <span><?php esc_html_e(get_field('contact_us_phone'))?></span></p>
						</div>
						<div class="email">
							<p class="email-okanagan-cbd iv-wp-from-right">EMAIL: <span><?php esc_html_e(get_field('contact_us_email'))?></span></p>
						</div>
					</div>
					<div class="form-contact">
						<div class="contact-form">
							<?php $contact_form_shortcode = get_field('contact_form_shortcode'); ?>
							<?php echo do_shortcode($contact_form_shortcode); ?>
						</div>
					</div>
				</div>
			</div>
		
		</section>
		<!-- endregion from contact-us -->
	</div>
	<div class="container separator end-of-page thick"></div>
<?php get_footer(); ?>