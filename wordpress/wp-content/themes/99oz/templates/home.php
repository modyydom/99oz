<?php
/*
 * template name: Home page
 */
get_header();
?>
	<div class="home-page-wrapper">

    <?php

    while (have_rows('home_sections')) {

      the_row();

      switch (get_row_layout()) {
        case 'hero_section':
          $image = get_sub_field('image');
          $title = get_sub_field('title');
          $social_links = get_sub_field('social_links');
          ?>

					<section class="hero">
						<div class="container">
							<div class="row">
								<div class="col-12 col-md-6 offset-md-1">
									<img src="<?=esc_url($image['url'])?>" alt="<?php esc_attr_e($image['alt']) ?>" class="hero-image iv-wp-from-bottom">
								</div>
								<h1 class="title">
									<span class="oz99-primary-color part-1 iv-wp-from-top"><?php esc_html_e($title['line_1']) ?></span>
									<span class="oz99-primary-color part-2 iv-wp-from-right"><?php esc_html_e($title['line_2']) ?></span>
								</h1>
							</div>
							<div class="social-links">
								<a href="<?=esc_url($social_links['instagram_link']['url'])?>" target="<?=esc_attr($social_links['instagram_link']['target'] ? $social_links['instagram_link']['target'] : '_self')?>"
								   class="link oz99-black-color iv-wp-from-right"><i
										class="icon oz99-primary-background-color oz99-off-white-color fab fa-instagram"></i> <?php esc_html_e($social_links['instagram_link']['title'] ?
                                                                                                                             $social_links['instagram_link']['title'] : __('INSTAGRAM', 'oz99')) ?></a>

								<a href="<?=esc_url($social_links['facebook_link']['url'])?>" target="<?=esc_attr($social_links['facebook_link']['target'] ? $social_links['facebook_link']['target'] : '_self')?>"
								   class="link oz99-black-color iv-wp-from-right"><i
										class="icon oz99-primary-background-color oz99-off-white-color fab fa-facebook-f"></i> <?php esc_html_e($social_links['facebook_link']['title'] ?
                                                                                                                              $social_links['facebook_link']['title'] : __('FACEBOOK', 'oz99')) ?></a>

								<a href="<?=esc_url($social_links['twitter_link']['url'])?>" target="<?=esc_attr($social_links['twitter_link']['target'] ? $social_links['twitter_link']['target'] : '_self')?>"
								   class="link oz99-black-color iv-wp-from-right"><i
										class="icon oz99-primary-background-color oz99-off-white-color fab fa-twitter"></i> <?php esc_html_e($social_links['twitter_link']['title'] ?
                                                                                                                           $social_links['twitter_link']['title'] : __('TWITTER', 'oz99')) ?></a>
							</div>
							<div class="divider oz99-primary-background-color iv-wp-from-top"></div>
						</div>
					</section>

          <?php
          break;

        case 'who_we_r_section':
          $image = get_sub_field('bg_image');

          ?>

					<section class="who-we-r">
						<div class="bg">

							<img src="<?=esc_url($image['url'])?>" alt="<?php esc_attr_e($image['atr']) ?>" class="no-zoom">
							<div class="filter oz99-primary-background-color"></div>
						</div>
						<div class="divider oz99-off-white-background-color iv-wp-from-bottom"></div>
						<div class="container">
							<div class="row justify-content-center align-items-center">
								<div class="col-12 col-lg-6">
									<h2 class="title oz99-black-color iv-wp-from-top"><?php esc_html_e(get_sub_field('title')) ?></h2>
									<h3 class="subtitle oz99-off-white-color iv-wp-from-left"><?php esc_html_e(get_sub_field('subtitle')) ?></h3>
								</div>
								<div class="col-12 col-lg-4 offset-lg-1">
									<p class="description oz99-off-white-color iv-wp"><?php the_sub_field('description') ?></p>
								</div>
							</div>
						</div>
					</section>

          <?php
          break;

        case 'why_cans_section':
          $image = get_sub_field('image')
          ?>

					<section class="why-cans">
						<div class="container">
							<div class="row">
								<div class="col-12 col-lg-4 offset-lg-1">
									<h4 class="why section-name oz99-primary-color iv-wp-from-top"><?php esc_html_e(get_sub_field('title')) ?></h4>
									<div class="row justify-content-between reasons">
                    <?php
                    while (have_rows('reasons')) {
                      the_row();
                      $icon = get_sub_field('icon');
                      ?>
											<div class="reason col-10 offset-2 col-md-5 offset-md-1 col-lg-10 offset-lg-2">
												<img src="<?=esc_url($icon['url'])?>" alt="<?php esc_attr_e($icon['atr']) ?>" class="icon m-0 iv-wp-from-left">
												<div class="text oz99-black-color">
													<h3 class="title"><?=get_sub_field('title')?></h3>
													<p class="description iv-wp"><?php esc_html_e(get_sub_field('description')) ?></p>
												</div>
											</div>
                      <?php
                    }
                    ?>
									</div>
								</div>
								<div class="col-12 col-lg-6 offset-lg-1 iv-wp-from-right">
									<img src="<?=esc_url($image['url'])?>" alt="<?php esc_attr_e($image['atr']) ?>" class="large-image">
								</div>
							</div>
						</div>
					</section>

          <?php
          break;

        case 'testimonials_section':
          $image = get_sub_field('bg_image')
          ?>

					<section class="testimonials">
						<div class="bg">
							<img src="<?=esc_url($image['url'])?>" alt="<?php esc_attr_e($image['atr']) ?>" class="no-zoom">
							<div class="filter oz99-black-background-color"></div>
						</div>

						<div class="container">
							<div class="row justify-content-center align-items-center">
								<div class="col-11 offset-1">
									<h4 class="section-name oz99-primary-color iv-wp-from-left"><?php esc_html_e(get_sub_field('small_title')) ?></h4>
									<h2 class="title oz99-off-white-color"><?php the_sub_field('big_title') ?></h2>
								</div>
								<div class="col-10">
									<div class="testimonials-slider">
                    <?php
                    while (have_rows('testimonials')) {
                      the_row();
                      ?>
											<div class="testimonial iv-wp-from-bottom">
												<div class="double-quotes oz99-primary-color">“</div>
												<div class="text oz99-black-color"><?php the_sub_field('text') ?></div>
												<div class="author">
													<div class="name oz99-black-color"><?php esc_html_e(get_sub_field('name')) ?></div>
													<div class="location oz99-secondary-color"><?php esc_html_e(get_sub_field('location')) ?></div>
												</div>
											</div>
                      <?php
                    }
                    ?>
									</div>
								</div>
							</div>
						</div>
					</section>

          <?php
          break;

        case 'other_products_section':
          ?>
					<section class="product-big-slider">
						<div class="container">
							<div class="section-header"><?php the_sub_field('title') ?></div>


              <?php
              $args = array('post_type'      => 'product',
                            'posts_per_page' => -1);
              $loop = new WP_Query($args);
              if ($loop->have_posts()) {
                $data = array('images'      => [],
                              'description' => []);
                ?>
								<div class="row align-items-center justify-content-center">

                  <?php
                  ob_start();
                  while ($loop->have_posts()) : $loop->the_post();
                    array_push($data['images'], get_the_post_thumbnail_url(get_the_ID()));
                    //                    wc_get_product( id )
                    ?>

										<div class="main-details" id="product-<?php the_ID(); ?>">
                      <?php wc_get_template_part('content', 'single-product'); ?>
										</div>

                    <?php
                    array_push($data['description'], ob_get_contents());
                    ob_clean();
                  endwhile;
                  ob_end_clean();
                  ?>
									<div class="col-12 col-md-5 changing-position">
										<div class="photo">
											<div class="middle-image-container">
                        <?php foreach ($data['images'] as $image) : ?>
													<img src="<?=esc_url($image)?>" class="middle-image no-zoom" alt="">
                        <?php endforeach; ?>
											</div>
                      <?php
                      foreach ($data['images'] as $image) {
                        ?>
												<div class="left-slide">
													<img src="<?=esc_url($image)?>" class="no-zoom" alt="">
												</div>
												<div class="right-slide">
													<img src="<?=esc_url($image)?>" class="no-zoom" alt="">
												</div>
                        <?php
                      } ?>
										</div>
									</div>
									<div class="col-12 col-md-6 offset-md-1">
                    <?php foreach ($data['description'] as $description) {
                      echo $description;
                    } ?>
									</div>
								</div>
              <?php } else {
                /**
                 * Hook: woocommerce_no_products_found.
                 *
                 * @hooked wc_no_products_found - 10
                 */
                do_action('woocommerce_no_products_found');
              }


              wp_reset_postdata();

              ?>

						</div>
					</section>

          <?php
          break;

        case 'instagram_section':
          ?>

					<section class="instagram iv-wp-from-bottom">
						<div class="photos">

              <?php
              foreach (get_sub_field('gallery') as $image) {
                ?>
								<div class="photo">
									<img src="<?=esc_url($image['url'])?>" alt="<?php esc_attr_e($image['atr']) ?>" class="no-zoom">
									<div class="filter">
										<div class="overlay oz99-primary-background-color"></div>
										<i class="fab fa-instagram oz99-off-white-background-color oz99-primary-color"></i>
									</div>
								</div>
                <?php
              }
              ?>
						</div>
					</section>

          <?php
          break;
      }
    }
    ?>

	</div>
<?php
get_footer();
