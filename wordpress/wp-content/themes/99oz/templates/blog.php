<?php
/*
 * template name: Blog page
 */

get_header();
?>
	<div class="blog-page-wrapper">
		
		<section class="blog-filter container">
			<div class="title iv-wp">BLOG</div>
			<div class="filter iv-wp-from-top">
				<div data-filter="*" class="filter-item active">ALL</div>
				<?php
				$categories = get_categories();
				foreach ($categories as $category) { ?>
					<div data-filter=".<?php echo $category->slug; ?>" class="filter-item"><?php echo $category->name; ?></div>
				<?php }
				?>
			</div>
		</section>
		
		<div class="container">
			<div class="separator iv-wp-from-top">
				<div class="text">FEED</div>
			</div>
		</div>
		
		<section class="blog-feeds container  iv-wp-from-bottom">
			<div class="row">
				
				<form action="" class="news-letter col-12 col-md-6 col-lg-4 iv-wp-from-right">
					<h3 class="title">JOIN OUR NEWSLETTER</h3>
					<input type="email" placeholder="email address">
					<button>
						SIGN UP
						<i class="fal fa-long-arrow-right"></i>
					</button>
				</form>

<?php
$args = array('post_type' => 'post',
              'posts_per_page' => -1,
              'order'          => 'ASC',
              'paged'     => get_query_var('paged') ? get_query_var('paged') : 1);
$the_query = new WP_Query($args);
if ($the_query->have_posts()) {
	while ($the_query->have_posts()) {
		$the_query->the_post();
		$categories = get_the_category(get_the_ID());
		?>
				<div class="isotope-element <?php foreach ($categories as $category) {
					echo $category->slug.' ';
				} ?> col-12 col-md-6 col-lg-4">
					<div class="blog-feed free-height cbd-card colored-top-border brown3">
						<div class="img-container">
							<img src="assets/images/blog/blog-img-1.png" alt="">
						</div>
						<div class="card-bottom">
							<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
							<div class="date-reading">
								<span class="date"><?php echo get_the_date( 'M j Y' ); ?></span>
								&nbsp///&nbsp
								<span class="reading"><?php echo $categories[0]->name; ?></span>
							</div>
							<p><?php echo wp_trim_words(get_the_content(),20) ?></p>
							<a href="<?php the_permalink(); ?>"><i class="fal fa-long-arrow-right"></i></a>
						</div>
					</div>
				</div>
		
		<?php
	}  // end while
	wp_reset_postdata();
} // end if
?>
			</div>
		</section>
	
	</div>
	<div class="container separator end-of-page thick"></div>
<?php
get_footer();