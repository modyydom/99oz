<?php
/*
 * template name: FAQ's page
 */

get_header();
?>
	<div class="faqs-page-wrapper">
		<div class="container">
			<div class="row">
        
        <?php
        while (have_rows('faqs_sections')) {
          the_row();
          
          switch (get_row_layout()) {
            case 'title_area':
              $call = get_sub_field('call');
              $mail = get_sub_field('mail');
              ?>
							
							<div class="col-md-6 col-12">
								<div class="left-column">
									<div class="page-info">
										<h1 class="page-title iv-wp-from-bottom oz99-black-color"><?php the_sub_field('title') ?></h1>
                    
                    <?php if (get_sub_field('subtitle')) { ?>
											<div class="page-text iv-wp-from-bottom"><?php esc_html_e(get_sub_field('subtitle')) ?></div>
                    <?php } ?>
									</div>
									<div class="page-contact">
										<div class="call-us iv-wp-from-left">
											<h5 class="title"><?php esc_html_e($call['title']) ?></h5>
											<a href="">
												<div class="phone"><i class="fas fa-phone-square"></i><?php esc_html_e($call['number']) ?></div>
											</a>
										</div>
										<div class="call-us iv-wp-from-left">
											<h5 class="title"><?php esc_html_e($mail['title']) ?></h5>
											<a href="">
												<div class="phone"><i class="fas fa-phone-square"></i><?php esc_html_e($mail['email']) ?></div>
											</a>
										</div>
									</div>
								</div>
							</div>
              
              <?php
              break;
            case 'questions_area':
              ?>
							
							<div class="col-md-6 col-12">
								<div class="right-column">
                  <?php
                  $toggledClass = ' toggled';
                  while (have_rows('questions')) {
                    the_row();
                    ?>
										<div class="iv-wp-from-bottom question-wrapper<?=$toggledClass?>">
											<div class="question-head toggleable-slide-up-header">
												<h4><?php esc_html_e(get_sub_field('question')) ?></h4>
												<i class="far fa-plus"></i>
												<i class="far fa-minus"></i>
											</div>
											<div class="question-body toggleable-slide-up-body"><?php esc_html_e(get_sub_field('answer')) ?></div>
										</div>
                    <?php
                    $toggledClass = '';
                  }
                  ?>
								</div>
							</div>
              
              <?php
              break;
            
          }
        }
        ?>
			
			</div>
		</div>
	</div>
<?php
get_footer();