<?php
/**
 * Template Name: Text Template
 */
get_header();
if (have_posts()) {
	the_post();
}
?>
<div class="text-template-page-wrapper">
	<?php if(get_field('page_cover')){ ?>
	<div class="text-template-hero iv-wp-from-top" style="background-image: url('<?php echo get_field('page_cover_background'); ?>')">
		<h2><?php echo get_field('page_cover_title'); ?></h2>
	</div>
	<?php } ?>
	<div class="container">
		
		<div class="text-wrapper iv-wp-from-bottom">
			<?php the_content(); ?>
		</div>
	
	</div>
</div>
<div class="container separator end-of-page thick"></div>

<?php get_footer(); ?>



