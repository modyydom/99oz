<?php
/*
 * template name: About page
 */

get_header();
?>
  <div class="about-page-wrapper">
    <?php
    // loop through the rows of data
    while (have_rows('about_sections')) {
      
      the_row();
      
      switch (get_row_layout()) {
        case 'upper_section':
          $blog = get_sub_field('blog');
          ?>
          <div class="general-page-header">
            <div class="container">
              <div class="row justify-content-md-between justify-content-center">
                <div class="col-11 col-md-6 col-lg-7 col-xl-7">
                  <div class="text-with-icon d-flex">
                    <div class="the-text col-11 col-md-10">
                      <h4 class="iv-wp-from-top"><?php esc_html_e(get_sub_field('title'))?></h4>
                      <p class="iv-wp"><?php esc_html_e(get_sub_field('paragraph'))?></p>
                    </div>
                  </div>
                </div>
                <div class="col-10 col-md-6 col-lg-5 col-xl-5 iv-wp-from-right">
                  <div class="img-text-card colored-top-border <?=convert_color_to_class($blog['color'])?> d-flex iv-wp-from-right hover-arrow">
                    <div class="the-image overflow-hidden">
                      <img src="<?=$blog['image']['url']?>" alt="<?php esc_attr_e($blog['image']['atr'])?>">
                    </div>
                    <div class="the-text">
                      <p><?php esc_html_e($blog['small_text'])?></p>
                      <a href="<?php esc_attr_e($blog['link'])?>"><?php esc_html_e($blog['big_text'])?><i class="fal fal fa-long-arrow-right"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
  
          <?php
          break;
        case 'who_we_are':
          $bg_img = get_sub_field('background_image');
          $product_img = get_sub_field('product_image');
          $text = get_sub_field('text');
          ?>
          <section class="who-we-are">
            <div class="row">
              <div class="col-md-6 no-padding">
                <div class="div-img iv-wp-from-bottom overflow-hidden">
                  <img src="<?=get_sub_field('image')['url']?>" alt="<?php esc_attr_e(get_sub_field('image')['atr']) ?>" class="logo">
                </div>
              </div>
              <div class="col-md-6 no-padding">
                <div class="div-text">
                  <h3 class="iv-wp-from-right"><?php esc_html_e(get_sub_field('title')) ?></h3>
                  <p class="iv-wp"><?php esc_html_e(get_sub_field('paragraph', false)) ?></p>
                </div>
              </div>
            </div>
          </section>
          <?php
          break;
        case 'our_mission':
          ?>
          
          <section class="our-mission">
            <div class="container">
              <h3 class=" iv-wp-from-top"><?php esc_html_e(get_sub_field('title')) ?></h3>
              <div class="text  col-lg-8 col-md-10">
                <p class="iv-wp"><?php esc_html_e(get_sub_field('paragraph', false)) ?></p>
              </div>
            </div>
          </section>
          <?php
          break;
        case 'cbd_attributes':
          ?>
          <section class="cba-attributes">
            <div class="container">
              <h3 class="iv-wp-from-top">CBD ATTRIBUTES</h3>
              <div class="col-lg-8 col-md-12 make-m-auto">
                <ul>
                  <li class="left-text li-opacity-border-top">
                    <div class="div-icon iv-wp-from-left">
                      <img src="<?=get_template_directory_uri()?>/assets/images/about/icon1.png" alt="">
                    </div>
                    <div class="div-text iv-wp-from-left">
                      <h4 class="first-color">PROVED ORIGIN</h4>
                      <p>Lorem ipsum dolor sit amet poenam consectitur</p>
                    </div>
                  </li>
                  <li class="right-text">
                    <div class="div-text iv-wp-from-right">
                      <h4 class="first-color">TRUE SAFETY</h4>
                      <p>Lorem ipsum dolor sit amet poenam consectitur</p>
                    </div>
                    <div class="div-icon iv-wp-from-right">
                      <img src="<?=get_template_directory_uri()?>/assets/images/about/icon2.png" alt="">
                    </div>
                  </li>
                </ul>
                <ul>
                  <li class="left-text">
                    <div class="div-icon iv-wp-from-left">
                      <img src="<?=get_template_directory_uri()?>/assets/images/about/icon3.png" alt="">
                    </div>
                    <div class="div-text iv-wp-from-left">
                      <h4 class="secound-color">PROVED ORIGIN</h4>
                      <p>Lorem ipsum dolor sit amet poenam consectitur</p>
                    </div>
                  </li>
                  <li class="right-text">
                    <div class="div-text iv-wp-from-right">
                      <h4 class="secound-color">TRUE SAFETY</h4>
                      <p>Lorem ipsum dolor sit amet poenam consectitur</p>
                    </div>
                    <div class="div-icon iv-wp-from-right">
                      <img src="<?=get_template_directory_uri()?>/assets/images/about/icon4.png" alt="">
                    </div>
                  </li>
                </ul>
                <ul>
                  <li class="left-text">
                    <div class="div-icon iv-wp-from-left">
                      <img src="<?=get_template_directory_uri()?>/assets/images/about/icon5.png" alt="">
                    </div>
                    <div class="div-text iv-wp-from-left">
                      <h4 class="third-color">PROVED ORIGIN</h4>
                      <p>Lorem ipsum dolor sit amet poenam consectitur</p>
                    </div>
                  </li>
                  <li class="right-text">
                    <div class="div-text iv-wp-from-right">
                      <h4 class="third-color">TRUE SAFETY</h4>
                      <p>Lorem ipsum dolor sit amet poenam consectitur</p>
                    </div>
                    <div class="div-icon iv-wp-from-right">
                      <img src="<?=get_template_directory_uri()?>/assets/images/about/icon6.png" alt="">
                    </div>
                  </li>
                </ul>
                <ul class="ul-without-border">
                  <li class="left-text li-opacity-border-bottom">
                    <div class="div-icon iv-wp-from-left">
                      <img src="<?=get_template_directory_uri()?>/assets/images/about/icon7.png" alt="">
                    </div>
                    <div class="div-text iv-wp-from-left">
                      <h4 class="last-color">PROVED ORIGIN</h4>
                      <p>Lorem ipsum dolor sit amet poenam consectitur</p>
                    </div>
                  </li>
                  <li class="right-text">
                    <div class="div-text iv-wp-from-right">
                      <h4 class="last-color">TRUE SAFETY</h4>
                      <p>Lorem ipsum dolor sit amet poenam consectitur</p>
                    </div>
                    <div class="div-icon iv-wp-from-right">
                      <img src="<?=get_template_directory_uri()?>/assets/images/about/icon8.png" alt="">
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </section>
          <?php
          break;
      }
    }
    ?>
  </div>
	<div class="container separator end-of-page thick"></div>
<?php
get_footer();