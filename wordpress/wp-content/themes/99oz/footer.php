<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Okanagan
 */

?>
<footer>
	<div class="container">
		<hr>
		<div class="row">
			<div class="col-12 col-md-6">
				<div class="links">
					<?php
					if (have_rows('footer_links', 'option')):
						while (have_rows('footer_links', 'option')) : the_row();
							$the_link = get_sub_field('the_link');
							?>
								<a class="link oz99-black-color" target="<?php echo $the_link['target']; ?>" href="<?php echo $the_link['url']; ?>"><?php echo $the_link['title']; ?></a>
						<?php endwhile;
					endif;
					?>
				</div>
				
				<div class="social-links">
					<a href="<?php echo get_field('instagram_link', 'option'); ?>" class="link oz99-black-color"><i class="icon oz99-primary-background-color oz99-off-white-color fab fa-instagram"></i></a>
					<a href="<?php echo get_field('facebook_link', 'option'); ?>" class="link oz99-black-color"><i class="icon oz99-primary-background-color oz99-off-white-color fa fa-facebook-f"></i></a>
					<a href="<?php echo get_field('twitter_link', 'option'); ?>" class="link oz99-black-color"><i class="icon oz99-primary-background-color oz99-off-white-color fab fa-twitter"></i></a>
				</div>
			</div>
			<div class="col-12 col-md-6">
				<form action="" class="newsletter">
					<div class="title"><?php echo get_field('footer_newsletter_title', 'option'); ?></div>
					<input type="email" name="email" id="email" class="oz99-black-border-color oz99-black-color" placeholder="YOUR EMAIL">
					<button class="solid" type="submit">JOIN NEWSLETTER</button>
				</form>
			</div>
			<div class="col-12 oz99-black-color copy-rights">
				<i class="fal fa-copyright"></i>
				<?php echo get_field('footer_copyright_text', 'option'); ?>
			</div>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
<script>
    $('.prices-boxes input:checkbox').change(function(){
        if($(this).is(":checked")) {
            $(this).closest('.price-radio-checbox-filter').addClass("checked");
        } else {
            $(this).closest('.price-radio-checbox-filter').removeClass("checked");
        }
    });
    $('.effects-filter-checkboxes input:checkbox').change(function(){
        if($(this).is(":checked")) {
            $(this).closest('.label-for-checkbox').addClass("label-active");
        } else {
            $(this).closest('.label-for-checkbox').removeClass("label-active");
        }
    });
    $('.usages-filter-checkboxes input:checkbox').change(function(){
        if($(this).is(":checked")) {
            $(this).closest('.label-for-checkbox').addClass("label-active");
        } else {
            $(this).closest('.label-for-checkbox').removeClass("label-active");
        }
    });
    jQuery(function($){
        $('#filter').submit(function(){
            var filter = $('#filter');
            $.ajax({
                url:filter.attr('action'),
                data:filter.serialize(), // form data
                type:filter.attr('method'), // POST
                beforeSend:function(xhr){
                   // filter.find('button').text('Processing...'); // changing the button label
                },
                success:function(data){
                 //   filter.find('button').text('Apply filter'); // changing the button label back
                    $('#response').html(data); // insert data
                }
            });
            return false;
        });
    });

    var selector = '.reset-btn';
    $(selector).on('click', function(){
        $('.label-for-checkbox').removeClass('label-active');
        $('.price-radio-checbox-filter').removeClass('checked');
    });
</script>


</body>
</html>
