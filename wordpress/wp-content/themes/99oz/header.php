<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Okanagan
 */

?>
<!doctype html>
<!--suppress CheckEmptyScriptTag -->
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>"/>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="loading">
	<div id="loading-center-absolute">
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
	</div>
</div>

<header>
	<?php $tbg = get_field('top_bar_background-color', 'option'); ?>
	
	<div style="background-color: <?php echo $tbg; ?>" class="top_bar oz99-off-white-color oz99-secondary-background-color">
		<?php echo get_field('top_bar_text', 'option'); ?>
	</div>
	<div class="bottom_bar">
		<div class="container">
			<div class="header-content">
				<a href="<?php echo site_url(); ?>">
					<div class="logo-wrapper">
						<img alt="<?php echo get_field('the_logo', 'option')['alt']; ?>" src="<?php echo get_field('the_logo', 'option')['url']; ?>">
					</div>
				</a>
				<nav class="nav-bar collapse navbar-collapse">
					<ul class="main-nav-links">
						<?php
						if (have_rows('the_header_menu', 'option')):
							while (have_rows('the_header_menu', 'option')) : the_row();
								$menu_item = get_sub_field('menu_item');
								$is_it_has_dropdown = get_sub_field('is_it_has_dropdown');
								$show_logout_link = get_sub_field('show_logout_link');
								?>
								<?php if ($is_it_has_dropdown) { ?>
									
									<li class="nav-item okanagan-dropdown-parent">
										<a class="oz99-black-color" href="<?php echo $menu_item['url']; ?>"><?php echo $menu_item['title']; ?> <i class="far fa-angle-down"></i></a>
										<div class="okanagan-drop-down">
											<ul>
												<?php
												if (have_rows('dropdown_items', 'option')):
													while (have_rows('dropdown_items', 'option')) : the_row();
														$dropdown_item = get_sub_field('dropdown_item');
														?>
														<li>
															<a target="<?php echo $dropdown_item['target']; ?>" href="<?php echo $dropdown_item['url']; ?>"><?php echo $dropdown_item['title']; ?></a>
														</li>
													<?php endwhile;
												endif;
												?>
												<?php if ($show_logout_link && is_user_logged_in()) { ?>
													<li><a href="<?php echo wp_logout_url(home_url()); ?>">logout</a></li>
												<?php } ?>
											</ul>
										</div>
									</li>
								
								<?php } else { ?>
									<li class="nav-item">
										<a class="oz99-black-color" target="<?php echo $menu_item['target']; ?>" href="<?php echo $menu_item['url']; ?>" data-hover="<?php echo $menu_item['title']; ?>"><?php echo $menu_item['title']; ?></a>
									</li>
								<?php } ?>
							
							<?php endwhile;
						endif;
						?>
					</ul>
				</nav>
				
				<div class="navbar-collapse-wrapper d-lg-none">
					<div class="navbar-collapse-toggler">
						<span></span>
						<span></span>
						<span></span>
						<span></span>
					</div>
				</div>
			</div>
		</div>
		<div class="cart-wrapper oz99-primary-background-color">
			<a href="<?php echo wc_get_cart_url(); ?>">
				<i class="far fa-shopping-cart"></i>
				<span class="oz99-off-white-color cart-quantity custom-cart-number"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
			</a>
		</div>
	</div>
</header>
