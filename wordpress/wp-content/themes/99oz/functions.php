<?php
/**
 * Okanagan functions and definitions
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Okanagan
 */

if (!function_exists('okanagan_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    /*Ignore Woocommerce Default style*/

    function okanagan_setup()
    {
        /*Woocommerce support theme*/
        add_theme_support('woocommerce');
        //		add_filter('woocommerce_enqueue_styles', '__return_false');

        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on Okanagan, use a find and replace
         * to change 'okanagan' to the name of your theme in all the template files.
         */
        load_theme_textdomain('okanagan', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');


        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array('menu-1' => esc_html__('Primary', 'okanagan'),));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array('search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('okanagan_custom_background_args', array('default-color' => 'ffffff',
            'default-image' => '',)));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support('custom-logo', array('height' => 250,
            'width' => 250,
            'flex-width' => true,
            'flex-height' => true,));
    }
endif;
add_action('after_setup_theme', 'okanagan_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function okanagan_content_width()
{
    // This variable is intended to be overruled from themes.
    // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
    // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
    $GLOBALS['content_width'] = apply_filters('okanagan_content_width', 640);
}

add_action('after_setup_theme', 'okanagan_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function okanagan_widgets_init()
{
    register_sidebar(array('name' => esc_html__('Sidebar', 'okanagan'),
        'id' => 'sidebar-1',
        'description' => esc_html__('Add widgets here.', 'okanagan'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',));
}

add_action('widgets_init', 'okanagan_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function okanagan_scripts()
{
    wp_enqueue_style('okanagan-style', get_stylesheet_uri());

    wp_enqueue_script('okanagan-bundle', get_template_directory_uri() . '/js/bundle.js', array(), '1.0.0', true);
    wp_enqueue_style('font-awesome', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

    wp_enqueue_style('mo-style', get_template_directory_uri() . '/mo_style.css');
    wp_enqueue_style('okanagan-woocommerce2', get_template_directory_uri() . '/woocommerce2.css');

}

add_action('wp_enqueue_scripts', 'okanagan_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
    require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if (class_exists('WooCommerce')) {
    require get_template_directory() . '/inc/woocommerce.php';
}

/**
 * return rating stars
 *
 * @param $number
 */
function display_rating_stars($number)
{
    // Convert any entered number into a float
    // Because the rating can be a decimal e.g. 4.5
    $number = number_format($number, 1);
    $tempFraction = fmod($number, 1) * 10;

    if (round($tempFraction) < 3) {
        $number = floor($number);
    } elseif (round($tempFraction) >= 8) {
        $number = floor($number) + 1;
    } else {
        $number = floor($number) + .5;
    }
    // Get the integer part of the number
    $intpart = floor($number); //4

    // Get the fraction part
    $fraction = $number - $intpart; //0.6

    // Rating is out of 5
    // Get how many stars should be left blank
    $unrated = 5 - ceil($number);

    // Populate the full-rated stars
    if ($intpart <= 5) {
        for ($i = 0; $i < $intpart; $i++) {
            echo '<i class="fas fa-circle"></i>';
        }
    }

    // Populate the half-rated star, if any
    if ($fraction == 0.5) {
        echo '<i class="fas fa-adjust"></i>';
    }

    // Populate the unrated stars, if any
    if ($unrated > 0) {
        for ($j = 0; $j < $unrated; $j++) {
            echo '<i class="far fa-circle"></i>';
        }
    }
}


function convert_color_to_class($color)
{
    switch ($color) {
        case '#71716f':
            return 'brown1';
        case '#8b7769':
            return 'brown2';
        case '#be8d7e':
            return 'brown3';
        case '#dabea8':
            return 'brown4';
    }
}

/**
 * @param $wcv_price
 * @param $product
 *
 * @return string
 */
function wc_variation_price_range($wcv_price, $product)
{

    $prefix = sprintf('%s: ', __('From', 'wcvp_range'));

    $wcv_reg_min_price = $product->get_variation_regular_price('min', true);
    $wcv_min_sale_price = $product->get_variation_sale_price('min', true);
    $wcv_max_price = $product->get_variation_price('max', true);
    $wcv_min_price = $product->get_variation_price('min', true);

    $wcv_price = ($wcv_min_sale_price == $wcv_reg_min_price) ? wc_price($wcv_reg_min_price) : '<del>' . wc_price($wcv_reg_min_price) . '</del>' . '<ins>' . wc_price($wcv_min_sale_price) . '</ins>';

    return ($wcv_min_price == $wcv_max_price) ? $wcv_price : sprintf('%s%s', $prefix, $wcv_price);
}

add_filter('woocommerce_variable_sale_price_html', 'wc_variation_price_range', 10, 2);
add_filter('woocommerce_variable_price_html', 'wc_variation_price_range', 10, 2);
add_filter('woocommerce_product_variation_title_include_attributes', '__return_false');

remove_action('woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10);
add_action('woocommerce_checkout_before_order_review_heading', 'woocommerce_checkout_coupon_form', 10);


remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 4);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);


remove_action('woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20);
add_action('woocommerce_single_product_summary', 'get_product_type_html', 20);
add_action('woocommerce_single_product_summary', 'get_product_description_html', 25);
add_action('woocommerce_checkout_shipping', 'woocommerce_checkout_payment', 1);

add_filter('woocommerce_billing_fields', 'oz99_change_billing_fields_order', 10, 1);

function get_product_type_html()
{
    global $product;

    $strain = get_the_terms($product->get_id(), 'pa_strain');

    if (!$strain) {
        return;
    }

    $strain = $strain[0];
    $image = get_field('strain_logo', $strain);

    ?>
    <div class="type oz99-black-color">
        <span class="iv-wp-from-left">TYPE: </span>
        <a href="<?php echo esc_url(get_term_link($strain)); ?>">
            <img class="iv-wp-from-right" src="<?= esc_url($image['url']) ?>" alt="<?php esc_attr_e($image['alt']) ?>">
        </a>
    </div>
    <?php
}

function get_product_description_html()
{
    global $product;
    ?>
    <div class="description oz99-black-color iv-wp"><?php the_content(); ?></div><?php
}

function oz99_change_billing_fields_order($billing_fields)
{

    // 'first_name' - 10
    // 'last_name' - 20
    // 'company' - 30
    // 'country' - 40
    // 'address_1' - 50
    // 'address_2' - 60
    // 'city' - 70
    // 'state' - 80
    // 'postcode' - 90
    $billing_fields['billing_country']['select2'] = false;
    $billing_fields['billing_country']['priority'] = 30;
    $billing_fields['billing_phone']['priority'] = 31;
    $billing_fields['billing_email']['priority'] = 32;
    $billing_fields['billing_state']['priority'] = 34;
    $billing_fields['billing_state']['label'] = 'Province';
    $billing_fields['billing_postcode']['priority'] = 35;
    $billing_fields['billing_state']['label'] = 'Postal code';
    $billing_fields['billing_city']['priority'] = 42;
    $billing_fields['billing_city']['label'] = 'City';

    return $billing_fields;
}

add_filter('woocommerce_shipping_fields', 'oz99_change_shipping_fields_order', 10, 1);

function oz99_change_shipping_fields_order($shipping_fields)
{

    // 'first_name' - 10
    // 'last_name' - 20
    // 'company' - 30
    // 'country' - 40
    // 'address_1' - 50
    // 'address_2' - 60
    // 'city' - 70
    // 'state' - 80
    // 'postcode' - 90
    $shipping_fields['shipping_country']['select2'] = false;
    $shipping_fields['shipping_country']['priority'] = 30;
    $shipping_fields['shipping_state']['priority'] = 31;
    $shipping_fields['shipping_state']['label'] = 'Province';
    $shipping_fields['shipping_postcode']['priority'] = 32;
    $shipping_fields['shipping_state']['label'] = 'Postal code';
    $shipping_fields['shipping_city']['priority'] = 42;
    $shipping_fields['shipping_city']['label'] = 'City';

    return $shipping_fields;
}

add_filter('woocommerce_gateway_icon', 'bbloomer_remove_what_is_paypal', 10, 2);

function bbloomer_remove_what_is_paypal($icon_html, $gateway_id)
{
    if ('paypal' == $gateway_id) {
        $icon_html = preg_replace("/<a[^>]*>([^<]+)<\/a>/", "", $icon_html);
    }

    return $icon_html;
}


// include ACF into the theme
// Define path and URL to the ACF plugin.
define('MY_ACF_PATH', get_stylesheet_directory() . '/includes/acf/');
define('MY_ACF_URL', get_stylesheet_directory_uri() . '/includes/acf/');

// Include the ACF plugin.
include_once(MY_ACF_PATH . 'acf.php');

// Customize the url setting to fix incorrect asset URLs.
add_filter('acf/settings/url', 'my_acf_settings_url');
function my_acf_settings_url($url)
{
    return MY_ACF_URL;
}

// (Optional) Hide the ACF admin menu item.
add_filter('acf/settings/show_admin', 'my_acf_settings_show_admin');
function my_acf_settings_show_admin($show_admin)
{
    return true;
}

// enable options tab in ACF
if (function_exists('acf_add_options_page')) {
    acf_add_options_page();
}

add_action('acf/input/admin_footer', 'load_javascript_on_admin_edit_post_page');

// remove p tag from wysiwyg
remove_filter('acf_the_content', 'wpautop');


/*update cart number with ajax*/

add_filter('woocommerce_add_to_cart_fragments', 'iconic_cart_count_fragments', 10, 1);


function iconic_cart_count_fragments($fragments)
{

    $fragments['span.custom-cart-number.oz99-off-white-color.cart-quantity'] = '<span class="oz99-off-white-color cart-quantity custom-cart-number">' . WC()->cart->get_cart_contents_count() . '</span>';

    return $fragments;

}

add_action('wp_enqueue_scripts', 'agentwp_dequeue_stylesandscripts', 100);

function agentwp_dequeue_stylesandscripts()
{
    if (class_exists('woocommerce')) {
        wp_dequeue_style('selectWoo');
        wp_deregister_style('selectWoo');

        wp_dequeue_script('selectWoo');
        wp_deregister_script('selectWoo');

    }
}

add_action('template_redirect', 'custom_template_redirect');

function custom_template_redirect()
{

    if (is_shop()) :

        wp_enqueue_script('change-url', get_template_directory_uri() . '/js/change-url.js');

    endif;
}


/**
 * Add quantity field on the shop page.
 */
function ace_shop_page_add_quantity_field()
{
    /** @var WC_Product $product */
    $product = wc_get_product(get_the_ID());
    if (!$product->is_sold_individually() && 'variable' != $product->get_type() && $product->is_purchasable()) {
        woocommerce_quantity_input(array('min_value' => 1,
            'max_value' => $product->backorders_allowed() ? '' : $product->get_stock_quantity()));
    }
}

add_action('woocommerce_after_shop_loop_item', 'ace_shop_page_add_quantity_field', 8);
/**
 * Add required JavaScript.
 */
function ace_shop_page_quantity_add_to_cart_handler()
{
    wc_enqueue_js('
		$(".woocommerce .products").on("click", ".quantity input", function() {
			return false;
		});
		$(".woocommerce .products").on("change input", ".quantity .qty", function() {
			var add_to_cart_button = $(this).parents( ".product" ).find(".add_to_cart_button");
			// For AJAX add-to-cart actions
			add_to_cart_button.data("quantity", $(this).val());
			// For non-AJAX add-to-cart actions
			add_to_cart_button.attr("href", "?add-to-cart=" + add_to_cart_button.attr("data-product_id") + "&quantity=" + $(this).val());
		});
		// Trigger on Enter press
		$(".woocommerce .products").on("keypress", ".quantity .qty", function(e) {
			if ((e.which||e.keyCode) === 13) {
				$( this ).parents(".product").find(".add_to_cart_button").trigger("click");
			}
		});
	');
}

add_action('init', 'ace_shop_page_quantity_add_to_cart_handler');


function add_fake_error($posted)
{
    if ($_POST['confirm-order-flag'] == "1") {
        wc_add_notice(__("custom_notice", 'fake_error'), 'error');
    }
}

add_action('woocommerce_after_checkout_validation', 'add_fake_error');

add_action('wp_head', 'hide_sidebar');
function hide_sidebar()
{
    if (!is_shop()) { ?>
        <style type="text/css">
            #secondary {
                display: none;
            }
        </style>
        <?php
    }
}

//remove_filter ('acf_the_content', 'wpautop');
function acf_wysiwyg_remove_wpautop()
{
    remove_filter('acf_the_content', 'wpautop');
}

add_action('acf/init', 'acf_wysiwyg_remove_wpautop', 15);


function oz_acf_get_products_attribute_names($field)
{

    global $woocommerce;

    $attr_tax = wc_get_attribute_taxonomies();


    //  $taxonomies = get_taxonomies();

    //  foreach ( $taxonomies as $taxonomy ) {
    $choices = array();
    foreach ($attr_tax as $tax) {
        $choices[$tax->attribute_name] = $tax->attribute_label;
//    $taxonomy =  $tax->get_slug();
//    $choices[$taxonomy] = $taxonomy.'lalala';

    }
//
    $field['choices'] = $choices;

    wp_reset_postdata();

    return $field;

}

add_filter('acf/load_field/key=field_5ce54073affe2', 'oz_acf_get_products_attribute_names');


/*
// Custom Ajax Request for Shop sidebar!

add_action('wp_ajax_myfilter', 'misha_filter_function'); // wp_ajax_{ACTION HERE}
add_action('wp_ajax_nopriv_myfilter', 'misha_filter_function');

function misha_filter_function(){
	$args = array(
		'orderby' => 'date', // we will sort posts by date
		'order'	=> $_POST['date'] // ASC or DESC
	);

	// for taxonomies / categories
	if( isset( $_POST['categoryfilter'] ) )
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'pa_price',
				'field' => 'id',
				'terms' => $_POST['categoryfilter']
			)
		);

	// if post thumbnail is set
	if( isset( $_POST['featured_image'] ) && $_POST['featured_image'] == 'on' )
		$args['meta_query'][] = array(
			'key' => '_thumbnail_id',
			'compare' => 'EXISTS'
		);
	// if you want to use multiple checkboxed, just duplicate the above 5 lines for each checkbox

	$query = new WP_Query( $args );

	if( $query->have_posts() ) :
		while( $query->have_posts() ): $query->the_post();
			echo '<h2>' . $query->post->post_title . '</h2>';
		endwhile;
		wp_reset_postdata();
	else :
		echo 'No posts found';
	endif;

	die();
}
*/
// That's how we'll get Attribues names in Shop page, guess what! it's so easy!
function getEffectsAttributes()
{
    $terms = get_terms(array('taxonomy' => 'pa_effects', 'hide_empty' => '0'));
    foreach ($terms as $term) {
        ?>
        <label class="label-for-checkbox container <?php if (isset($_POST['submit']) && in_array($term->name, $_POST['effects'])) {
            echo "label-active";
        } ?>">
            <input id="<?php echo $term->name; ?>" type="checkbox" name="effects[]" class="regular-checkbox" value="<?php echo $term->name; ?>" <?php if (isset($_POST['submit']) && in_array($term->name, $_POST['effects'])) {
                echo "checked='checked'";
            } ?>>
            <input for="<?php echo $term->name; ?>" type="checkbox">
            <span class="checkmark"></span>
            <?php echo $term->name; ?>
        </label>
        <?php
    }
}

function getUsagesAttributes()
{
    $terms = get_terms(array('taxonomy' => 'pa_usages', 'hide_empty' => '0'));
    foreach ($terms as $term) {
        ?>
        <label class="label-for-checkbox container <?php if (isset($_POST['submit']) && in_array($term->name, $_POST['effects'])) {
            echo "label-active";
        } ?>">
            <input id="<?php echo $term->name; ?>" type="checkbox" name="usages[]" value="<?php echo $term->name; ?>" class="regular-checkbox" <?php if (isset($_POST['submit']) && in_array($term->name, $_POST['usages'])) {
                echo "checked='checked'";
            } ?>>
            <input for="<?php echo $term->name; ?>" type="checkbox">
            <span class="checkmark"></span>
            <?php echo $term->name; ?></label>
        <?php
    }
}

function getPricesAttributes()
{
    $terms = get_terms(array('taxonomy' => 'pa_price', 'hide_empty' => '0', 'order' => 'DSC'));
    $number_id = 1;
    foreach ($terms as $term) {
        ?>
        <div class="price-radio-checbox-filter <?php if (isset($_POST['price_attributes']) && in_array($term->term_id, $_POST['price_attributes'])) {
            echo "checked";
        } ?>">
            <input id="number_id" value="<?php echo $number_id; ?>" hidden/>
            <label><?php echo $term->name; ?>
                <input type="checkbox" name="price_attributes[]" value="<?php echo $term->term_id; ?>" class="price_attribute_checbox"></div>
        </label>
        <?php
        $number_id++;
    }
}

function getTypesAttributes()
{
    $terms = get_terms(array('taxonomy' => 'pa_types', 'hide_empty' => '0', 'order' => 'DSC'));
    foreach ($terms as $term) {
        $type_term = 'term_' . $term->term_id;
        $term_id = $term->term_id;
        $type_image = get_field('type_image', $type_term);
        ?>
        <div class="type-radio-checbox-filter">
            <label class="">
                <input type="checkbox" name="types_attributes[]" value="<?php echo $term_id; ?>" <?php if (isset($_POST['submit']) && in_array($term_id, $_POST['types_attributes'])) {
                    echo "checked='checked'";
                } ?>>
                <img src="<?php echo $type_image; ?>">
            </label>
        </div>
        <?php
    }
}
// include custom jQuery
function shapeSpace_include_custom_jquery()
{

    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', array(), null, true);

}

add_action('wp_enqueue_scripts', 'shapeSpace_include_custom_jquery');


add_action('wp_ajax_myfilter', 'misha_filter_function'); // wp_ajax_{ACTION HERE}
add_action('wp_ajax_nopriv_myfilter', 'misha_filter_function');

function misha_filter_function(){

    if($_POST['effects']){
        $effects=$_POST['effects'];
    }else{
        $effects;
    }
    if($_POST['types_attributes']){
        $types=$_POST['types_attributes'];
    }else{
        $types;
    }
    if($_POST['price_attributes']){
        $prices=$_POST['price_attributes'];
    }else{
        $prices;
    }  if($_POST['usages']){
        $usages=$_POST['usages'];
    }else{
        $usages;
    }
    // The query
    $tax_query = array('relation' => 'OR');
    if (!empty($_POST['effects'])) {
        $tax_query[] = array(
            'taxonomy' => 'pa_effects',
            'field' => 'slug',
            'terms' => $effects,
        );
    }
    if (!empty($_POST['types_attributes'])) {
        $tax_query[] = array(
            'taxonomy' => 'pa_types',
            'field' => 'term_id',
            'terms' => $types,
        );
    }
    if (!empty($_POST['price_attributes'])) {
        $tax_query[] = array(
            'taxonomy' => 'pa_price',
            'field' => 'id',
            'terms' => $prices,
        );
    }
    if (!empty($_POST['usages'])) {
        $tax_query[] = array(
            'taxonomy' => 'pa_usages',
            'field' => 'slug',
            'terms' => $usages,
        );
    }
    $products = new WP_Query(
        array(
            'post_type' => 'product',
            'posts_per_page' => '-1',
            'tax_query' => $tax_query,
            'order'=>'ASC',
        )
    );

// The Loop
    if ($products->have_posts()):
    $total=$products->found_posts;
        ?>
        <div class="col-12 results-number oz99-black-color iv-wp-from-left">Results: <?php echo $total; ?></div>
        <div class="products row woocommerce-result-container">
        <?php
        while ($products->have_posts()):
        $products->the_post();

            do_action('woocommerce_shop_loop');

            wc_get_template_part('content', 'product');

        endwhile;
        wp_reset_postdata();
    else :
        echo '<h2>No products found</h2>';
    endif;

    die();
}


// New Quantity buttons to make it work after update cart with Ajax
add_action( 'wp_footer', 'bbloomer_cart_refresh_update_qty' );
function bbloomer_cart_refresh_update_qty() {
      ?>
      <script type="text/javascript">
        jQuery(document).on('click', '.plus2', function() {
     var currentVal = parseInt(jQuery(this).next(".qty").val());
     if (currentVal != NaN)
        {
            jQuery(this).next(".qty").val(currentVal + 1);
        	jQuery( '.woocommerce-cart-form :input[name="update_cart"]' ).prop( 'disabled', false );
            jQuery(".update-cart").trigger('click');

        }
    });
      </script>
      <script type="text/javascript">
        jQuery(document).on('click', '.minus2', function() {
         var currentValminus = parseInt(jQuery(this).prev(".qty").val());
     if (currentValminus != NaN && currentValminus > 1 )
        {
            jQuery(this).prev(".qty").val(currentValminus - 1);
      		jQuery( '.woocommerce-cart-form :input[name="update_cart"]' ).prop( 'disabled', false );
            jQuery(".update-cart").trigger('click');
        }
    });
      </script>

      <?php
  }
  // Simple code to make items number updates automatically in Cart Page
  add_filter( 'woocommerce_add_to_cart_fragments', 'iconic_cart_count_fragments_shop', 10, 1 );

  function iconic_cart_count_fragments_shop($fragments)
{

    $fragments['span.shop-count-items'] = '<span class="shop-count-items">' . WC()->cart->get_cart_contents_count() . '</span>';

    return $fragments;

}
function CommentsQuery()
{
    define('DEFAULT_COMMENTS_PER_PAGE', 4);

    $id = get_the_ID();

    $page = (get_query_var('page')) ? get_query_var('page') : 1;;


    $limit = DEFAULT_COMMENTS_PER_PAGE;

    $offset = ($page * $limit) - $limit;

    $param = array(

        'status' => 'approve',

        'offset' => $offset,

        'post_id' => $id,

        'number' => $limit,

    );
    $total_comments = get_comments(array('orderby' => 'post_date',

        'order' => 'DESC',

        'post_id' => $id,

        'status' => 'approve',

        'parent' => 0));

    $pages = ceil(count($total_comments) / DEFAULT_COMMENTS_PER_PAGE);
    $comments = get_comments($param);

    // Comment Loop
    if ($comments) {
        foreach ($comments as $comment) {
            $rating = get_comment_meta($comment->comment_ID, 'rating', true);

            ?>

            <div class="col-12 col-md-6">
                <div class="review">
                    <div class="title oz oz99-secondary-color iv-wp-from-top"><?php esc_html_e(get_the_title()) ?></div>
                    <div class="rating oz99-primary-color"><?php display_rating_stars($rating) ?></div>
                    <div class="review-body oz99-black-color iv-wp"><?= $comment->comment_content ?></div>
                    <div class="author-details iv-wp-from-left">
                        <div class="name oz99-black-color"><?php esc_html_e($comment->comment_author) ?>
                        </div>
                        <?php
                    $d = "d/m/Y";
                    $comment_ID=$comment->comment_ID;
                    $comment_date = get_comment_date( $d, $comment_ID );
                        ?>
                        <div class="date oz99-secondary-color">, <?= $comment_date; ?></div>
                    </div>
                </div>
            </div>
        <?php }

    } else {
        echo 'No comments found.';
    }
    ?>
    <hr class="iv-wp-from-top">
    </div>
    <?php $current_page = max(1, get_query_var('paged')); ?>
    <?php

    $args = array(

        'base' => @add_query_arg('page', '%#%'),
        'format' => '?page=%#%',
        'total' => $pages,
        'current' => $page,
        'show_all' => true,
        'end_size' => 1,
        'mid_size' => 2,
        'prev_next' => true,
        'prev_text' => __('Previous'),
        'next_text' => __('Next'),
        'type' => 'plain');

    // ECHO THE PAGENATION
    echo '<div class="pagination-links">'.paginate_links($args);
    echo '</div>';
}
function pagination_bar( $custom_query ) {

    $total_pages = $custom_query->max_num_pages;
    $big = 999999999; // need an unlikely integer

    if ($total_pages > 1){
        $current_page = max(1, get_query_var('paged'));

        echo paginate_links(array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '?paged=%#%',
            'current' => $current_page,
            'total' => $total_pages,
        ));
    }
}
