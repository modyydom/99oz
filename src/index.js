import "normalize.css/normalize.css";

import "./styles/style.scss";
import "@fortawesome/fontawesome-pro/scss/fontawesome.scss";
import "@fortawesome/fontawesome-pro/scss/light.scss";
import "@fortawesome/fontawesome-pro/scss/regular.scss";
import "@fortawesome/fontawesome-pro/scss/solid.scss";
import "@fortawesome/fontawesome-pro/scss/brands.scss";
import "slick-carousel/slick/slick.scss";

import {header} from "./scripts/layouts";
import {home, singleProduct, shop, checkout} from "./scripts/pages";
import {runAllHelpingFunctions, WayPoints} from "./scripts/general";
import {SplitText, TweenMax} from "gsap/all";

const reInvokableFunction = ($document) => {
  const $ivwp = $document.find("[class ^=\"iv-wp\"], [class *= \" iv-wp\"]");
  $ivwp.addClass("iv-wp-animating");
  let filtered = $ivwp.filter(".iv-wp");
  
  TweenMax.set($ivwp, {autoAlpha: 0});
  
  if (filtered.length) {
    new SplitText(filtered, {type: "lines", linesClass: "split-line"});
  }
  
  $document.find(".initializing").removeClass("initializing");
  home($document);
  singleProduct($document);
  shop($document);
  checkout($document);
  runAllHelpingFunctions($document);
  setTimeout(WayPoints, 100);
};

$(document).ready(() => {
  $(window).on("resize", () => {
    Waypoint.refreshAll();
  });
  header.init();
  reInvokableFunction($(document));
  // SwupInit(reInvokableFunction);
  

});

$(window).on("load", function () {
  let $loading = $("#loading");
  $loading.fadeOut(500);
});