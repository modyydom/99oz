import "jquery";

export default class Header {
  $header;
  $burger;
  $close;
  $menu;
  $window = $(window);
  allowMenuCollapsing = this.$window.width() < 992;
  
  init = () => {
    if (!this.$header) { this.$header = $("header"); }
    if (!this.$burger) { this.$burger = this.$header.find(".navbar-collapse-wrapper"); }
    if (!this.$close) { this.$close = this.$header.find(".close"); }
    if (!this.$menu) { this.$menu = this.$header.find(".navbar-collapse"); }
    
    
    this.$window.on("resize", () => this.allowMenuCollapsing = this.$window.width() < 992);
    this.$burger.on("click", this.toggleMenu);
    $("body").on("click", e => {
      if (this.allowMenuCollapsing && !$(e.target).closest(".navbar-collapse-wrapper").length && !$(e.target).hasClass("navbar-collapse")) {
        this.closeMenu();
      }
      if ($(e.target).is(".nav-link, .nav-item, .auth-header-btn:not(.navbar-toggler)")) {
        $(".auth-drop-down").slideUp();
        this.closeMenu();
      }
    });
    
    /*region drop-down*/
    $(".navbar-toggler, .auth-drop-down .item").on("click", function () {
      $(".auth-drop-down").slideToggle();
    });
    /*endregion drop-down*/
  };
  
  closeMenu = () => {
    this.$menu.removeClass("show");
    this.$burger.removeClass("open");
  };
  toggleMenu = () => {
    this.allowMenuCollapsing ? this.$menu.toggleClass("show") : "";
    this.allowMenuCollapsing ? this.$burger.toggleClass("open") : "";
  };
  
}