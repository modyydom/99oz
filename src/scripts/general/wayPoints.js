import 'waypoints/lib/noframework.waypoints.min.js';
import {Power1, TimelineLite, TweenMax} from 'gsap/all';

const removeAnimatingClass = ($element, animatingClass = 'iv-wp-animating')=>{
  $element.removeClass(animatingClass);
};

export default () => {
  const $ivwp = $('[class ^="iv-wp"], [class *= " iv-wp"]');
  let filtered = $ivwp.filter('.iv-wp');
  filtered.each(function () {
    new Waypoint({
      element: this,
      handler: function (direction) {
        TweenMax.set($(this.element), {autoAlpha: 1});
        // TweenMax.set($(this.element).find(".split-line"), {autoAlpha: 1});
        TweenMax.staggerFrom($(this.element).find('.split-line'), .5, {yPercent: 100, autoAlpha: 0, ease: Power1.easeOut, onComplete:removeAnimatingClass, onCompleteParams:[$(this.element)]},.05);
        this.destroy();
      },
      offset: '85%',
    });
  });
  $ivwp.filter('.iv-wp-from-right').each(function () {
    new Waypoint({
      element: this,
      handler: function (direction) {
        TweenMax.set($(this.element), {autoAlpha: 1});
        TweenMax.from($(this.element), .7, {autoAlpha: 0, xPercent: 100, ease: Power1.easeOut, onComplete:removeAnimatingClass, onCompleteParams:[$(this.element)]});
        this.destroy();
      },
      offset: '85%',
    });
  });
  $ivwp.filter('.iv-wp-from-left').each(function () {
    new Waypoint({
      element: this,
      handler: function (direction) {
        TweenMax.set($(this.element), {autoAlpha: 1});
        TweenMax.from($(this.element), .7, {autoAlpha: 0, xPercent: -100, ease: Power1.easeOut, onComplete:removeAnimatingClass, onCompleteParams:[$(this.element)]});
        this.destroy();
      },
      offset: '85%',
    });
  });
  $ivwp.filter('.iv-wp-from-bottom').each(function () {
    new Waypoint({
      element: this,
      handler: function (direction) {
        TweenMax.set($(this.element), {autoAlpha: 1});
        TweenMax.from($(this.element), .7, {autoAlpha: 0, yPercent: 50, ease: Power1.easeOut, onComplete:removeAnimatingClass, onCompleteParams:[$(this.element)]});
        this.destroy();
      },
      offset: '85%',
    });
  });
  $ivwp.filter('.iv-wp-from-top').each(function () {
    new Waypoint({
      element: this,
      handler: function (direction) {
        TweenMax.set($(this.element), {autoAlpha: 1});
        TweenMax.from($(this.element), .7, {autoAlpha: 0, yPercent: -50, ease: Power1.easeOut, onComplete:removeAnimatingClass, onCompleteParams:[$(this.element)]});
        this.destroy();
      },
      offset: '85%',
    });
  });
}
