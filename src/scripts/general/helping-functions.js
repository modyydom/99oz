import "jquery";
import {TimelineMax} from "gsap/all";

export const productSlider = ($document, selector = ".product-big-slider") => {
  let $productSlider = $document.find(selector);
  let currentWidth = $(window).outerWidth();
  const $productSliderClone = $productSlider.clone();
  const SIDES = Object.freeze(
    {
      RIGHT: Symbol("RIGHT"),
      LEFT: Symbol("LEFT"),
      MIDDLE: Symbol("MIDDLE"),
      DETAILS: Symbol("DETAILS"),
    });
  const getIndex = (side, index, total) => {
    switch (side) {
      case SIDES.RIGHT:
        return (index + 1) % total;
      case SIDES.LEFT:
        return (index + total - 1) % total;
      case SIDES.MIDDLE:
      case SIDES.DETAILS:
        return index;
    }
  };
  
  //
  // $(window).on("resize", function () {
  //   const newWidth = $(this).outerWidth();
  //   if (!(currentWidth < 768 && newWidth < 768 || currentWidth >= 768 && newWidth >= 768)) {
  //     currentWidth = newWidth;
  //     $productSlider.find('.changing-position').replaceWith($productSliderClone.find('.changing-position').clone());
  //     // $productSlider = $productSliderClone;
  //     initProductSlider();
  //     Waypoint.refreshAll();
  //   }
  // });
  const initProductSlider = () => {
    $productSlider.each(function () {
      const $this = $(this);
      const $details = $this.find(".main-details");
      const $middleImages = $this.find(".middle-image");
      const $leftImages = $this.find(".left-slide");
      const $rightImages = $this.find(".right-slide");
      const $allImages = $middleImages.add($leftImages).add($rightImages);
      const slidesNumber = $middleImages.length;
      let currentIndex = 0;
      let isClickAllowed = true;
      
      
      const forwardsActive = () => {
        $allImages.css("z-index", (i, v) => (v + 1) % slidesNumber);
        $allImages.removeClass("active");
        $allImages.addClass("hidden");
        currentIndex = (currentIndex + 1) % slidesNumber;
        $middleImages.eq(getIndex(SIDES.MIDDLE, currentIndex, slidesNumber)).toggleClass("active hidden");
        $leftImages.eq(getIndex(SIDES.LEFT, currentIndex, slidesNumber)).toggleClass("active hidden");
        $rightImages.eq(getIndex(SIDES.RIGHT, currentIndex, slidesNumber)).toggleClass("active hidden");
      };
      const backwardsActive = () => {
        $allImages.css("z-index", (i, v) => (v + slidesNumber - 1) % slidesNumber);
        $allImages.removeClass("active");
        $allImages.addClass("hidden");
        currentIndex = (currentIndex + slidesNumber - 1) % slidesNumber;
        $middleImages.eq(getIndex(SIDES.MIDDLE, currentIndex, slidesNumber)).toggleClass("active hidden");
        $leftImages.eq(getIndex(SIDES.LEFT, currentIndex, slidesNumber)).toggleClass("active hidden");
        $rightImages.eq(getIndex(SIDES.RIGHT, currentIndex, slidesNumber)).toggleClass("active hidden");
      };
      
      $leftImages.each(function (i) {
        $(this).css("z-index", (slidesNumber + 1 - i) % slidesNumber);
      });
      
      $rightImages.each(function (i) {
        $(this).css("z-index", (slidesNumber - i) % slidesNumber);
      });
      
      $middleImages.each(function (i) {
        $(this).css("z-index", (slidesNumber - 1 - i) % slidesNumber);
      });
      $allImages.removeClass("active");
      $allImages.addClass("hidden");
      $middleImages.eq(getIndex(SIDES.MIDDLE, currentIndex, slidesNumber)).toggleClass("active hidden");
      $leftImages.eq(getIndex(SIDES.LEFT, currentIndex, slidesNumber)).toggleClass("active hidden");
      $rightImages.eq(getIndex(SIDES.RIGHT, currentIndex, slidesNumber)).toggleClass("active hidden");
      $this
        .on("click", ".left-slide", () => {
          
          if (isClickAllowed) {
            isClickAllowed = false;
            const isWindowSmall = $(window).outerWidth() < 768;
            const $currentMiddle = $middleImages.eq(getIndex(SIDES.MIDDLE, currentIndex, slidesNumber));
            const $currentRight = $rightImages.eq(getIndex(SIDES.RIGHT, currentIndex, slidesNumber));
            const $nextRight = $rightImages.eq(getIndex(SIDES.RIGHT, currentIndex + 1, slidesNumber));
            const $currentLeft = $leftImages.eq(getIndex(SIDES.LEFT, currentIndex, slidesNumber));
            const productsTl = new TimelineMax({
              paused: true,
              onComplete: () => {
                productsTl.progress(0);
                productsTl.kill(0);
                forwardsActive();
                $details.eq(getIndex(SIDES.DETAILS, currentIndex, slidesNumber)).slideDown(200, () => isClickAllowed = true);
              },
            });
            
            if (isWindowSmall) {
              $currentRight.setObject = {x: 0, y: 0, xPercent: -100, yPercent: -50, opacity: 0.5, className: "-=active"};
              $currentRight.toObject = {width: "60%", xPercent: -50, left: "50%", opacity: 1};
              $currentMiddle.setObject = {x: 0, y: 0, xPercent: -50, yPercent: -50, opacity: 1, className: "-=active"};
              $currentMiddle.toObject = {xPercent: 0, left: 0, zIndex: slidesNumber + 1, width: "20%", opacity: 0.5};
              
            } else {
              $currentRight.setObject = {x: 0, y: 0, xPercent: 0, yPercent: -50, opacity: 0.5, className: "-=active"};
              $currentRight.toObject = {width: ($currentRight.outerWidth() / 0.11 / 12 * 5) - 16, left: 8, opacity: 1};
              $currentMiddle.setObject = {x: 0, y: 0, xPercent: 0, yPercent: -50, opacity: 1, className: "-=active"};
              $currentMiddle.toObject = {x: -($currentMiddle.outerWidth() * .28) - 8, zIndex: slidesNumber + 1, width: "28%", opacity: 0.5};
            }
            
            
            productsTl
              .set($currentMiddle, $currentMiddle.setObject)
              .set($currentRight, $currentRight.setObject)
              .set($currentLeft, {className: "-=active"})
              .set($currentLeft, {opacity: 0.5})
              .set($nextRight, {className: "-=hidden"})
              .set($nextRight, {opacity: 0})
              .to($currentMiddle, 0.3, $currentMiddle.toObject, "start")
              .to($currentRight, 0.3, $currentRight.toObject, "start")
              .to($currentLeft, 0.3, {opacity: 0}, "start")
              .to($nextRight, 0.3, {opacity: 0.5}, "start");
            
            $details.eq(getIndex(SIDES.DETAILS, currentIndex, slidesNumber)).slideUp(200, () => productsTl.play());
          }
          
        })
        
        .on("click", ".right-slide", () => {
          
          if (isClickAllowed) {
            isClickAllowed = false;
            const isWindowSmall = $(window).outerWidth() < 768;
            const $currentMiddle = $middleImages.eq(getIndex(SIDES.MIDDLE, currentIndex, slidesNumber));
            const $currentRight = $rightImages.eq(getIndex(SIDES.RIGHT, currentIndex, slidesNumber));
            const $currentLeft = $leftImages.eq(getIndex(SIDES.LEFT, currentIndex, slidesNumber));
            const $nextLeft = $leftImages.eq(getIndex(SIDES.LEFT, currentIndex - 1, slidesNumber));
            const productsTl = new TimelineMax({
              paused: true,
              onComplete: () => {
                productsTl.progress(0);
                productsTl.kill(0);
                backwardsActive();
                $details.eq(getIndex(SIDES.DETAILS, currentIndex, slidesNumber)).slideDown(200, () => isClickAllowed = true);
              },
            });
            
            if (isWindowSmall) {
              $currentLeft.setObject = {x: 0, y: 0, xPercent: 0, yPercent: -50, opacity: 0.5, className: "-=active"};
              $currentLeft.toObject = {width: "60%", xPercent: -50, left: "50%", opacity: 1};
              $currentMiddle.setObject = {x: 0, y: 0, xPercent: -50, yPercent: -50, opacity: 1, className: "-=active"};
              $currentMiddle.toObject = {xPercent: -100, left: "100%", zIndex: slidesNumber + 1, width: "20%", opacity: 0.5};
              
            } else {
              $currentLeft.setObject = {x: 0, y: 0, xPercent: -100, yPercent: -50, opacity: 0.5, className: "-=active"};
              $currentLeft.toObject = {width: ($currentRight.outerWidth() / 0.11 / 12 * 5) - 16, left: 8, xPercent: 0, opacity: 1};
              $currentMiddle.setObject = {x: 0, y: 0, xPercent: 0, yPercent: -50, opacity: 1, className: "-=active"};
              $currentMiddle.toObject = {x: ($currentMiddle.outerWidth() + 16) * 2.4 - 8, zIndex: slidesNumber + 1, width: "28%", opacity: 0.5};
            }
            
            
            productsTl
              .set($currentMiddle, $currentMiddle.setObject)
              .set($currentLeft, $currentLeft.setObject)
              .set($currentRight, {className: "-=active"})
              .set($currentRight, {opacity: 0.5})
              .set($nextLeft, {className: "-=hidden"})
              .set($nextLeft, {opacity: 0})
              .to($currentMiddle, 0.3, $currentMiddle.toObject, "start")
              .to($currentLeft, 0.3, $currentLeft.toObject, "start")
              .to($currentRight, 0.3, {opacity: 0}, "start")
              .to($nextLeft, 0.3, {opacity: 0.5}, "start");
            
            $details.eq(getIndex(SIDES.DETAILS, currentIndex, slidesNumber)).slideUp(200, () => productsTl.play());
            
          }
          
        });
    });
    
  };
  initProductSlider();
};

export const select2Init = ($document, selector = "select:not([data-select2-id])") => {
  $document.find(selector).each(function () {
    if ($(this).hasClass("in-modal")) {
      $(this).select2({
        dropdownParent: $("#myModal"),
      });
    } else {
      $(this).select2();
    }
    
    // Change the arrow on the Select
    setTimeout(
      () => $document.find(".select2-selection__arrow b")
        .replaceWith(" <span class=\"select2-separator\">|</span><i class=\"fas fa-caret-down\"></i>"),
      500);
  });
};


export const toggleableSlideUpInit = ($document, selector = ".toggleable-slide-up-header") => {
  $document.find(selector)
    .filter(":not(label)").on("click", function (e) {
    if (e.target === e.currentTarget) {
      $(this).next(".toggleable-slide-up-body").slideToggle(400, function () {
        Waypoint.refreshAll();
        $(this).parent().toggleClass("toggled");
        $(this).toggleClass("toggleable-slide-up-header-opened");
      });
    }
  });
  $document.on("click", "label.checkbox-container.toggleable", function (e) {
    if (e.target === e.currentTarget) {
      setTimeout(() => {
        const $input = $(this).children("input[type=radio]");
        const $uncheckedLabels = $document.find(`:has(input[name=${$input.attr("name")}]:not(:checked))`);
        $input.parent().next(".toggleable-slide-up-body").slideDown(400, function () {
          Waypoint.refreshAll();
          $(this).parent().addClass("toggled");
          $(this).addClass("toggleable-slide-up-header-opened");
        });
        
        
        $uncheckedLabels.next(".toggleable-slide-up-body").slideUp(400, function () {
          Waypoint.refreshAll();
          $(this).parent().removeClass("toggled");
          $(this).removeClass("toggleable-slide-up-header-opened");
        });
      }, 100);
    }
  });
  $document.find("label.checkbox-container.toggleable:has(input:checked)").trigger("click");
};

const numberSelectorFunctionality = ($document, selector = ".number-selector") => {
  const isShop = !!$document.find("body.post-type-archive-product").length;
  $document.find(selector)
    .on("click", ".plus", function () {
      const $input = $(this).siblings("input");
      const max = parseInt($input.attr("max"));
      if ((!max && max !== 0) || max < 0 || $input.val() < max) {
        console.log(isShop);
        $input.val((i, v) => +v + 1);
        if (isShop) {
          $input.closest(".quantity").siblings(".add_to_cart_button").data("quantity", $input.val()).attr("data-quantity", $input.val());
        }
      }
      
    })
    .on("click", ".minus", function () {
      const $input = $(this).siblings("input");
      const min = parseInt($input.attr("min")) ? Math.abs(parseInt($input.attr("min"))) : 1;
      
      if (min && $input.val() > min) {
        $input.val((i, v) => +v - 1);
        if (isShop) {
          $input.closest(".quantity").siblings(".add_to_cart_button").data("quantity", $input.val()).attr("data-quantity", $input.val());
        }
      }
    });
  
};

export const runAllHelpingFunctions = ($document) => {
  select2Init($document);
  toggleableSlideUpInit($document);
  numberSelectorFunctionality($document);
  productSlider($document);
};