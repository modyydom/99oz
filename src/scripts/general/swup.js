import Swupjs from 'swupjs';
import {Power2, TimelineMax} from 'gsap/all';

const swupOptions = {
  elements: ['[class$="-page-wrapper"]'],
  LINK_SELECTOR: 'a:not([data-no-swup]):not([href = "javascript:;"]):not([data-fancybox]):not([data-fancybox]):not([data-fancybox-download])',
  cache: false,
  preload: false,
  debugMode: false,
  animateHistoryBrowsing: true,
  animations: {
    '*': {
      out: function (next) {
        let tl = new TimelineMax({onComplete: next});
        tl.to('[class$="-page-wrapper"],footer', .5, {autoAlpha: 0, ease: Power2.easeOut});
      },
      in: function (next) {
        let tl = new TimelineMax({onComplete: next});
        tl.set('footer', {autoAlpha: 1});
        tl.from('[class$="-page-wrapper"]', .5, {autoAlpha: 0, ease: Power2.easeIn}, 'sameTime');
        tl.from('footer', 0.5, {autoAlpha: 0, ease: Power2.easeIn, immediateRender: false}, 'sameTime');
      },
    },
  },
};

export default (theReinvokedFunctions) => {
  let first = true;
  document.addEventListener('swup:pageView', function (event) {
    
    const $newDocument = $(event.target);
    theReinvokedFunctions($newDocument);
    setTimeout(function () {
      Waypoint.refreshAll();
    }, 500);
  });
  const swup = new Swupjs(swupOptions);
}


