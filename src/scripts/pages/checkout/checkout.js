export const checkout = ($document) => {
  //region breadcrumb click
  $document.find(".wizard-nav").on("click", ".wizard-item.valid:not(.active)", function () {
    $("#confirm-order-flag").val("1");
    const data_value = $(this).toggleClass("active valid").siblings().removeClass("active").end().data("number");
    $document.find(".information-step")
      .slideUp(500, function () {
        $(this).removeClass("active").siblings(`[data-number=${data_value}]`)
          .slideDown(500, function () {$(this).addClass("active");});
      });
  });
  //endregion breadcrumb click
  
  // const $marker = $("<i class=\"removed-input-marker\"></i>");
  $document.find("#place_order")
    .on("click", function () {
      // console.log("submit");
      $("#confirm-order-flag").val("");
      // const $inputs = $(this).closest(".information-step").siblings().find(".input-wrapper :input");
      // $inputs.each(function () {
      //   const $this = $(this);
      //   if (!$this[0].$marker) {
      //     const $clonedMarker = $marker.clone();
      //     $this.before($clonedMarker);
      //     $this[0].$marker = $clonedMarker;
      //     $this.detach();
      //   }
      // });
      // console.log($inputs);
      // setTimeout(() => $inputs.each(function () {
      //   console.log($inputs);
      //   console.log(this.$marker);
      //   $(this).insertAfter(this.$marker);
      // }), 1000);
      
      // return true; // ensure form still submits
    });
  
  
  let needMore = false;
  let throttle = false;
  const throttleFunction = () => {
    if (needMore) {
      needMore = false;
      setTimeout(throttleFunction, 100);
      return;
    }
    $document.find("label.checkbox-container.toggleable:has(input:checked)").trigger("click");
    throttle = false;
  };
  
  $(".woocommerce").on("DOMNodeInserted", function (e) {
    
    if ($(e.target).attr("id") !== "place_order") {
      if (!throttle) {
        throttle = true;
        setTimeout(throttleFunction, 500);
      } else {
        needMore = true;
      }
    }
    
    if ($(e.target).is(":has(.woocommerce-error)")) {
      
      const $error = $(e.target).find(".woocommerce-error li");
  
      const $wizardItem = $document.find(".wizard-nav .wizard-item.active");
      if ($error.length === 1) { // Validation Passed (Just the Fake Error Exists)
        // $('html').animate({scrollTop:0}, '100');
        $document.find(".information-step")
          .slideUp(500, function () {
            $(this).removeClass("active")
              .next().slideDown(500, function () {$(this).addClass("active");});
          });
        
        $error.closest(".woocommerce-error").remove();
        
        $wizardItem
          .removeClass("active invalid").addClass("valid")
          .next().addClass("active");
        
      } else { // Validation Failed (Real Errors Exists, Remove the Fake One)
        
        $wizardItem.addClass('invalid');
        $error.each(function () {
          const error_text = $(this).text();
          if (error_text.trim() === "custom_notice") {
            $(this).css("display", "none");
          }
        });
      }
    }
  });
};