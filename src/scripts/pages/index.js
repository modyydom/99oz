export * from './faqs';
export * from './home';
export * from './single-product';
export * from './cart';
export * from './shop';
export * from './checkout';