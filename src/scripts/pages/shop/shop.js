import "jquery";

export const shop = ($document) => {
  const $filterWidget = $document.find('form.filter-widget');
  $document.find('.open-filters').on('click', function () {
    $(this).siblings('form').slideDown();
  });
  $document.find('.apply').on('click', function () {
    $(this).closest('form').slideUp();
  });
  $document.find('.close').on('click', function () {
    $(this).closest('form').slideUp();
  });
};