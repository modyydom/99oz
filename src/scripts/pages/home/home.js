import "slick-carousel";

export const home = ($document) => {
  const $instagram = $document.find("section.instagram .photos").slick({
    infinite: true,
    autoplay: true,
    rows: 0,
    slidesToShow: 4,
    slidesToScroll: 1,
    touchThreshold: 30,
    arrows: false,
    dots: false,
    // centerMode: true,
    // centerPadding: "60px",
    draggable: true,
    responsive: [
      {
        breakpoint: 991.98,
        settings: {
          slidesToShow: 3,
        },
      }, {
        breakpoint: 575.98,
        settings: {
          slidesToShow: 2,
        },
      },
    ],
  });
  const $testimonials = $document.find("section.testimonials .testimonials-slider").slick({
    infinite: false,
    autoplay: false,
    rows: 0,
    slidesToShow: 3,
    slidesToScroll: 3,
    touchThreshold: 30,
    arrows: true,
    dots: false,
    draggable: true,
    nextArrow: "<i class=\"oz-next-arrow oz99-off-white-border-color oz99-off-white-color fal fa-angle-right\"></i>",
    prevArrow: "<i class=\"oz-prev-arrow oz99-off-white-border-color oz99-off-white-color fal fa-angle-left\"></i>",
    responsive: [
      {
        breakpoint: 991.98,
        settings: {
          slidesToScroll: 2,
          slidesToShow: 2,
        },
      }, {
        breakpoint: 575.98,
        settings: {
          centerMode: true,
          centerPadding: "50px",
          slidesToShow: 1,
          arrows: false,
        },
      },
    ],
  }).on("setPosition", function () {
    $(this).find(".slick-slide")
      .outerHeight($(this).find(".slick-track").innerHeight() - 40);
  });
};