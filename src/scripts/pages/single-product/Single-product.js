import 'select2';
// import 'jquery-zoom';
// import 'jquery-colorbox';

// let panMethod = $(window).width() > 991.98 ? 'mouseover' : 'grab';
// const $zoomElement = $('.zoom');
// const options = {
//   magnify: 5,
//   on: panMethod,
//   callback: function () {
//     $(this).addClass('zoomed-img');
//   }
// };
export const singleProduct = ($document) => {
  const $productImages = $document.find("section.product-main .photos").slick({
    infinite: true,
    // autoplay:true,
    // rows:0,
    slidesToShow: 1,
    slidesToScroll: 1,
    touchThreshold: 30,
    arrows: false,
    dots: true,
    // centerMode: true,
    // centerPadding: "60px",
    draggable: true,
    // responsive: [
    //   {
    //     breakpoint: 991.98,
    //     settings: {
    //       slidesToShow: 3,
    //     },
    //   }, {
    //     breakpoint: 575.98,
    //     settings: {
    //       slidesToShow: 2,
    //     },
    //   },
    // ],
  });
};

// const initZoom = (init = false) => {
//
//   if ((init || panMethod === 'grab') && $(window).width() > 991.98) {
//     $zoomElement.trigger('zoom.destroy');
//     panMethod = 'mouseover';
//     setTimeout(() => $zoomElement.zoom({...options, on: panMethod}), 100);
//   } else if ((init || panMethod === 'mouseover') && !($(window).width() > 991.98)) {
//     $zoomElement.trigger('zoom.destroy');
//     panMethod = 'grab';
//     setTimeout(() => $zoomElement.zoom({...options, on: panMethod}), 100);
//   }
// };