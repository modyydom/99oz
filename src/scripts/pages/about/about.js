import 'slick-carousel';
import {Power1, TimelineMax} from 'gsap/all';


export const about = ($document) => {
  
  const $aboutTitle = $document.find('.about-page-wrapper .about-hero .location-content');
  
  const $aboutImages = $document.find('.about-page-wrapper .about-hero img.hero-bg');
  
  const $aboutDetails = $document.find('.about-page-wrapper section.about-us-details-container .details');
  
  let allowSwitchLocation = true;
  
  const $locationsLinks = $document.find('.about-page-wrapper .locations-container')
    .on('click', '.location:not(.active)', function () {
      if (allowSwitchLocation) {
        allowSwitchLocation = false;
        const locationNumber = $(this).addClass('active').siblings().removeClass('active').end().data('location-number');
        const $prevLocationDetails = $aboutTitle.filter('.active');
        const $nextLocationDetails = $aboutTitle.eq(locationNumber);
        $aboutImages.removeClass('active').eq(locationNumber).addClass('active');
        $aboutDetails
          .filter('.active').removeClass('active')
          .slideUp(500,
            () => $aboutDetails.eq(locationNumber)
                               .slideDown(500,
                                 function () {$(this).addClass('active')})
          );
        const tl1 = new TimelineMax({
          onComplete: () => {
            allowSwitchLocation = true;
            $nextLocationDetails.removeClass('animating');
            $prevLocationDetails.removeClass('active');
          }
        });
        $nextLocationDetails.addClass('active animating');
        tl1
          .staggerTo($prevLocationDetails.children('.location-title').find('.split-line'), .7, {xPercent: 100, autoAlpha: 0, ease: Power1.easeOut}, .1, 'start')
          .staggerTo($prevLocationDetails.children('.location-super-title').find('.split-line'), .7, {xPercent: -100, autoAlpha: 0, ease: Power1.easeOut}, .1, 'start')
          .staggerFromTo($nextLocationDetails.children('.location-title').find('.split-line'), .7, {xPercent: -100, autoAlpha: 0}, {xPercent: 0, autoAlpha: 1, ease: Power1.easeOut}, .1, 'start+=.3')
          .staggerFromTo($nextLocationDetails.children('.location-super-title').find('.split-line'), .7, {xPercent: 100, autoAlpha: 0}, {
            xPercent: 0,
            autoAlpha: 1,
            ease: Power1.easeOut
          }, .1, 'start+=.3');
      }
    });
  
};